\chapter{Introduction} \label{chapter:dspi_introduction}

An analysis of \BsDspi decays to measure the level to which they are flavour-specific was performed.
As discussed in Section~\ref{sec:theory_dspi}, it has previously been assumed that the decay is flavour-specific but this had never been explicitly tested.
The analysis builds upon the work done for the analysis of \BsDsK and follows a similar template.
The next few chapters will detail the method used to measure the \CP parameters of the \BsDspi decay,
particularly focussing on differences from the method used for the analysis of \BsDsK.

The main differences occur when parameterising the backgrounds of the distribution of reconstructed \Bs mass
and the method used for fitting the proper decay-time distribution which will be described in full in Chapter~\ref{chapter:time_fit}.

\chapter{Data selection} \label{chapter:data_selection}

\section{Data sample}

This analysis uses data from the 2011 run of \lhcb.
This comprises an integrated luminosity $\intlum{1.0 \,\invfb}$ of $\proton\proton$ collisions recorded at a centre-of-mass energy of $\sqrt{s}=\unit{7}{\tev}$.

\section{Simulated data}

Several samples of simulated data were created for the analysis, primarily for the use in event selection and background studies.
In each sample, a \B hadron is forced to decay to a specific final state as listed in Table~\ref{tab:mc_samples} along with the number of events generated for each channel.

\begin{table}[htbp]
 \centering
 \begin{tabular}{llr}
  \toprule
  \textbf{Sample} &         & \textbf{Sample size} \\
  \midrule
  \BsDspi         & \DsKKpi & 1052495 \\
  \BsDsstpi       & \DsKKpi & 524098  \\
  \BsDsrho        & \DsKKpi & 2019391 \\
  \BsDsstrho      & \DsKKpi & 1019191 \\
  \LbLcpi         & \LcpKpi & 2033496 \\
  \BdDrho         & \DKpipi & 2054494 \\
  \BdDstpi        & $\Dstar \to D\piz$, \DKpipi
                            & 1046498 \\
  \BdDpi          & \DKpipi & 1016198 \\
  \bottomrule
 \end{tabular}
 \caption{Simulated samples used during the analysis for selection and background studies.}
 \label{tab:mc_samples}
\end{table}

\section{Data selection}

The reconstruction of the \BsDspi signal events was performed in the same way as for the \BsDsK analysis.
The only difference is that rather than using a \kaon mass hypothesis for the bachelor when combining the tracks, a \pion mass hypothesis is used.

The requirements used in order to select the \BsDspi candidates are identical to those used in the \BsDsK analysis with the exception of the particle identification requirement on the bachelor track.
As both decays are kinematically similar, tuning of the BDT will perform well for selecting \BsDspi
since the BTD was explicitly not trained or optimised using any particle identification information about the bachelor.
A relatively tight requirement of $\dllkpi < 0$ is imposed on the bachelor track.

The distribution of the key variables for both real and simulated data after all the selection requirements 
are shown in Figures~\ref{fig:data_after_offline_selection} and \ref{fig:mc_after_offline_selection}.

\begin{figure}
  \centering
    \includegraphics[width=0.49\textwidth]{\currfiledir/Offline_Bs2DPi_merged_mass.pdf}
    \includegraphics[width=0.49\textwidth]{\currfiledir/Offline_Bs2DPi_merged_ctau.pdf}
  \caption{Distributions for \BsDspi data after the offline selection.}
  \label{fig:data_after_offline_selection}
\end{figure}

\begin{figure}
  \centering
    \includegraphics[width=0.49\textwidth]{\currfiledir/Offline_MC_Bs2DsPi_Ds2KKPi_mass.pdf}
    \includegraphics[width=0.49\textwidth]{\currfiledir/Offline_MC_Bs2DsPi_Ds2KKPi_ctau.pdf}
  \caption{Distributions for simulated \BsDspi after the offline selection.}
  \label{fig:mc_after_offline_selection}
\end{figure}
