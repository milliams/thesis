\chapter{Propagation of uncertainties in \boldmath\CP parameterisation} \label{appendix:error_propagation}

The results of the measurement of the \CP parameters in \BsDsK found that \Spar, \Sbpar, \Dpar and \Dbpar have approximately equal statistical uncertainties.
This is what would be expected given the formulation of the decay rates given in Equation~\ref{eq:decay_rates}.

During the \BsDspi analysis, these four \CP parameters were reparameterised as
\begin{equation}
 \begin{split}
  \avgs &= \frac{\Sf+\Sfbar}{2}, \\
  \avgd &= \frac{\Df+\Dfbar}{2},
 \end{split}
 \quad
 \begin{split}
  \deltas &= \frac{\Sf-\Sfbar}{2}, \\
  \deltad &= \frac{\Df-\Dfbar}{2} .
 \end{split}
 \label{eq:cp-parameters-avg-delta-repeat}
\end{equation}
with the aim of reducing the correlation between the parameters.
This was successful as can be seen in Table~\ref{tab:dspi-corr-stats} where all the correlations were found to be negligible.

\begin{table}
  \centering
    \begin{tabular}{cSSSS|S}
      \toprule
      \textbf{Parameters} & \avgs & \avgd & \deltas & \deltad & \textbf{Statistical uncertainty}\\
      \midrule
      \avgs               &  1    & -0.00 &  0.00   &  0.00   & 0.150 \\
      \avgd               & -0.00 &  1    & -0.00   & -0.00   & 0.098 \\
      \deltas             &  0.00 & -0.00 &  1      & -0.00   & 0.083 \\
      \deltad             &  0.00 & -0.00 & -0.00   &  1      & 0.050 \\
      \bottomrule
    \end{tabular}
  \caption{The correlations and statistical uncertainties for the \CP parameters in the fit to \BsDspi data. \label{tab:dspi-corr-stats}}
\end{table}

Naïvely, one might expect that since the original \CP parameters had similar statistical uncertainties
and since they are being combined linearly with identical coefficients
that the resultant uncertainties of \avgs, \avgd, \deltas and \deltad would be consistent with each other.

However, this does not take into account the correlations between the original \CP parameters which were found in the \BsDsK to be non-negligible.
A test fit is run over the \BsDspi data with the decay rate equations parameterised as \Spar, \Sbpar, \Dpar and \Dbpar in order to find the correlations and statistical uncertainties of the parameters. The results of this fit are shown in Table~\ref{tab:dspi-corr-stats-reparam}.

\begin{table}
  \centering
    \begin{tabular}{c*{4}{r}|c}
      \toprule
      \textbf{Parameters} & \Spar  & \Sbpar  & \Dpar  & \Dbpar  & \textbf{Statistical uncertainty}\\
      \midrule
      \Spar               &  1     &  0.535  & -0.053 & -0.039  & 0.17937 \\
      \Sbpar              &  0.535 &  1      & -0.036 & -0.032  & 0.16368 \\
      \Dpar               & -0.053 & -0.036  &  1     &  0.584  & 0.1113 \\
      \Dbpar              & -0.039 & -0.032  &  0.584 &  1      & 0.10862 \\
      \bottomrule
    \end{tabular}
  \caption{The correlations and statistical uncertainties for the \CP parameters in the fit to \BsDspi data. \label{tab:dspi-corr-stats-reparam}}
\end{table}

The uncertainties of the reparameterised \CP parameters, taking into account the correlations between the progenitor parameters are given by
\begin{alignat}{3}
 \sigma_{\avgs}^2   &= \frac{1}{4} \sigma_{\Sf}^2 &&+ \frac{1}{4} \sigma_{\Sfbar}^2 &&+ \frac{1}{2} \rho_{\Sf \Sfbar} \sigma_{\Sf} \sigma_{\Sfbar}, \nonumber \\
 \sigma_{\avgd}^2   &= \frac{1}{4} \sigma_{\Df}^2 &&+ \frac{1}{4} \sigma_{\Dfbar}^2 &&+ \frac{1}{2} \rho_{\Df \Dfbar} \sigma_{\Df} \sigma_{\Dfbar}, \nonumber \\
 \sigma_{\deltas}^2 &= \frac{1}{4} \sigma_{\Sf}^2 &&+ \frac{1}{4} \sigma_{\Sfbar}^2 &&- \frac{1}{2} \rho_{\Sf \Sfbar} \sigma_{\Sf} \sigma_{\Sfbar}, \nonumber \\
 \sigma_{\deltad}^2 &= \frac{1}{4} \sigma_{\Df}^2 &&+ \frac{1}{4} \sigma_{\Dfbar}^2 &&- \frac{1}{2} \rho_{\Df \Dfbar} \sigma_{\Df} \sigma_{\Dfbar},
\end{alignat}
where $\sigma$ is the statistical uncertainty of a given parameter and $\rho$ is the correlation coefficient between two parameters.
Using these equations and the values from Table~\ref{tab:dspi-corr-stats-reparam}, the expected statistical uncertainties are
\begin{alignat}{2}
 \sigma_{\avgs}    &= 0.150, \quad
 \sigma_{\avgd}   &&= 0.098, \nonumber \\
 \sigma_{\deltas}  &= 0.083, \quad
 \sigma_{\deltad} &&= 0.050.
\end{alignat}
which are exactly what were found when fitting with the \avgs, \avgd, \deltas, \deltad parameterisation as shown in Table~\ref{tab:dspi-corr-stats}.
