#! /usr/bin/python
# -*- coding: utf-8 -*-
import ROOT
from ROOT import TCanvas, gStyle, TLatex, TLine, TCurlyLine, TArc

ROOT.gROOT.Reset()
ROOT.gROOT.SetStyle("Plain")

gStyle.SetPaperSize(10.,10.)

gStyle.SetLineStyle(1)
gStyle.SetLineWidth(2)

def fix_pdf(filename):
	"""
	filename is without .pdf extension
	"""
	from subprocess import call
	command = "pdf2ps {filename}.pdf {filename}_temp.ps && \
	           ps2pdf {filename}_temp.ps {filename}_temp.pdf && \
	           pdfcrop {filename}_temp.pdf {filename}.pdf && \
	           rm --force {filename}_temp.pdf {filename}_temp.ps".format(filename=filename)
	#command = "/usr/bin/gs -o {filename}_temp.pdf -dPDFSETTINGS=/prepress -sDEVICE=pdfwrite {filename}.pdf && \
	#           pdfcrop {filename}_temp.pdf {filename}.pdf && \
	#           rm --force {filename}_temp.pdf".format(filename=filename)
	call([command], shell=True)

def draw_standard_tree(name, meson1, q11, q12, meson2, q21, q22, meson3, q31, q32, boson):
	c1 = TCanvas("c1_"+name, "c1_"+name ,500,500)
	c1.Range(0, 0, 500, 500)

	t = TLatex()
	t.SetTextAlign(22)
	t.SetTextSize(0.10)
	t.SetTextFont(12)

	l1 = TLine(100, 100, 400, 100)
	l1.Draw()
	l2 = TLine(100, 200, 400, 200)
	l2.Draw()
	l3 = TLine(300, 350, 400, 400)
	l3.Draw()
	l4 = TLine(300, 350, 400, 300)
	l4.Draw()

	cl = TCurlyLine(200, 200, 300, 350)
	cl.SetWavy()
	cl.Draw()

	t.DrawLatex(40, 150, meson1)
	t.DrawLatex(85, 200, q11)
	t.DrawLatex(85, 100, q12)

	t.DrawLatex(450, 150, meson2)
	t.DrawLatex(415, 200, q21)
	t.DrawLatex(415, 100, q22)

	t.DrawLatex(450, 350, meson3)
	t.DrawLatex(415, 300, q31)
	t.DrawLatex(415, 400, q32)

	t.DrawLatex(200,275, boson)

	filename = "feyn_" + name
	c1.SaveAs(filename+".pdf")
	fix_pdf(filename)

draw_standard_tree("BsDsmpip", "B^{0}_{s}", "#bar{b}", "s", "D^{-}_{s}", "#bar{c}", "s", "#pi^{+}", "#bar{d}", "u", "W^{+}")

draw_standard_tree("BsBetasppim", "B^{0}_{s}", "#bar{b}", "s", "X^{+}_{s}", "#beta", "s", "#pi^{-}", "d", "#bar{u}", "W^{-}")

draw_standard_tree("BsDsmKp", "B^{0}_{s}", "#bar{b}", "s", "D^{-}_{s}", "#bar{c}", "s", "K^{+}", "#bar{s}", "u", "W^{+}")

draw_standard_tree("BsDspKm", "B^{0}_{s}", "#bar{b}", "s", "D^{+}_{s}", "#bar{u}", "s", "K^{-}", "#bar{s}", "c", "W^{+}")


c2 = TCanvas("c2","",500,500)
c2.Range(0, 0, 500, 500)

t = TLatex()
t.SetTextAlign(22)
t.SetTextSize(0.10)
t.SetTextFont(12)

l1 = TLine(100, 100, 200, 100)
l1.Draw()
l2 = TLine(100, 400, 200, 400)
l2.Draw()
l3 = TLine(200, 100, 200, 400)
l3.Draw()

cl1 = TCurlyLine(200, 100, 300, 150)
cl1.SetWavy()
cl1.Draw()
cl2 = TCurlyLine(200, 400, 300, 350)
cl2.SetWavy()
cl2.Draw()

l4 = TLine(300, 350, 400, 400)
l4.Draw()
l5 = TLine(300, 350, 400, 300)
l5.Draw()
l6 = TLine(300, 150, 400, 200)
l6.Draw()
l7 = TLine(300, 150, 400, 100)
l7.Draw()

t.DrawLatex(40, 250, "B^{0}_{s}")
t.DrawLatex(85, 400, "#bar{b}")
t.DrawLatex(85, 100, "s")

t.DrawLatex(250, 250, "u,c,t")

t.DrawLatex(450, 150, "#pi^{-}")
t.DrawLatex(415, 200, "d")
t.DrawLatex(415, 100, "#bar{u}")

t.DrawLatex(450, 350, "D^{+}_{s}")
t.DrawLatex(415, 300, "c")
t.DrawLatex(415, 400, "#bar{s}")

#t.DrawLatex(275,400, "W^{+}")
#t.DrawLatex(275,100, "W^{-}")

filename = "feyn_" + "BsDsppim"
c2.SaveAs(filename+".pdf")
fix_pdf(filename)
