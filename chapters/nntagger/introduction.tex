This appendix contains work I performed during the course of my Ph.D. as part of my service work.
It was published as an internal \lhcb note as Ref~\cite{LHCb-INT-2011-046}.
Since it was originally an internal note, it contains some \lhcb jargon and also uses some different conventions such as $\hbar = c = 1$.

\section{Introduction}

Tagging is the process by which the quark flavour content of a B meson is determined. A flavour can sometimes be reconstructed from its decay daughters but due to mixing this is not necessarily the flavour it had at production. In order to study mixing effects or time-dependent \CP asymmetries, the initial flavour of the B meson at production must be determined. The taggers used at LHCb and their properties have been studied extensively \cite{Grabalosa:1456804,LHCb-CONF-2011-003}.

%\#\#Why we need tagging. Reference the existing tagging notes. LHCb-2007-058 perhaps\#\#

%There are a number of different techniques used to determine the flavour of this meson but they fall into two main categories. Ones which work directly on the signal B meson and those which give the flavour of the signal B meson based on the flavour of its sister B meson which was created from the other quark in the $b-\bar{b}$ quark pair (this is called the opposite-side B meson).

There are a number of different techniques used to determine the flavour of the reconstructed \B meson but they fall into two main categories. The Same Side (SS) taggers work on a particle created in the fragmentation process which created the signal \B meson: a pion in the case of $B^0$ or $B^+$ signal (or from a $B^{**}$ decay) and a kaon for $B_s^0$. The rest of taggers are those called Opposite Side (OS) taggers. These exploit the decay products of the other \B hadron produced in the event: a lepton (electron or muon) from semileptonic \B meson decays; a kaon from a $b\rightarrow c \rightarrow s$ chain and an overall charge of the secondary vertex.

Regardless of the side on which the tagger is working, most of the taggers work by selecting a single particle, the charge of which indirectly gives the flavour of the \B meson (the \emph{vertex charge} tagger works differently in that instead of selecting a single particle on the opposite side, it sums together the charges of the particles used to make the opposite-side \B vertex). This selection process works in a series of steps. Looping over all particles in the event (excluding those used in the reconstruction of the signal decay tree) a series of loose cuts are applied as listed in Table~\ref{tab:preliminary-cuts}. This reduces the number of candidates to a manageable level, increasing the tagging power. The impact parameter (IP) is the shortest perpendicular distance between a track and a vertex and the smallest of these is used as a discriminating variable here. The vertices used are the all primary vertices (PVs), excluding the primary vertex best associated with the signal \B meson candidate (these vertices are referred to as \emph{Pile-Up (PU) vertices}). These cuts are universal to all the taggers and those that pass are the initial tagging candidates for each of the taggers. More details about all the taggers implemented at LHCb are available in \cite{Grabalosa:1456804}.%The taggers are then called in turn, each returning either a predicted flavour of the signal B or a result saying that they were unable to conclusively evaluate a flavour. If they return a prediction they also return an estimate of how likely they are to be wrong (estimated mistag probability $\eta$).

\begin{table}[b]
 \centering
 \begin{tabular}{l|l}
 \textbf{Variable} & \textbf{Cut} \\ 
 \midrule
 Momentum & $>$ 2; $<$ \unit{200}{\gev} \\ 
 $\theta$ w.r.t the beamline & $>$ 0.012 \\ 
 Charge & not 0 \\ 
 Track type & Long or upstream \\ 
 \pt & $<$ \unit{10}{\gev} \\ 
 Smallest \mphi w.r.t all \B meson decay daughters & $>$ 0.005 \\ 
 Min IP w.r.t PU vertices & $>$ 3.0
 \end{tabular}
 \caption{The initial cuts that are applied to select tagging candidates}
 \label{tab:preliminary-cuts}
\end{table}

Each of the taggers makes their decision in their own way in that they apply a series of cuts to narrow down their selection. If no particles survive these cuts then the event is classed as untagged. If multiple particles pass the cuts then the one with the highest \pt is returned. Some kinematic variables from the surviving particle are then passed to a neural network which returns a prediction of how accurate the tag is likely to be (estimated mistag probability $\eta$). For example the muon tagger (works on the opposite side) cuts on the variables defined in Table~\ref{tab:standard-muon-cuts} and passes the variables in Table~\ref{tab:standard-muon-eta-nn} to its neural network.

\begin{table}[tb]
 \centering
 \begin{tabular}{l|l}
 \textbf{Variable} & \textbf{Cut} \\
 \midrule
 \pt & $>$ \unit{1.2}{\gev} \\
 Track \chisq per d.o.f. & $<$ 3.2 \\
 \dllmupi & $>$ 2.5 \\
 IP with the pile-up vertex/error & $>$ 3.0 \\
 Smallest \mphi w.r.t all decay daughters & $>$ 0.005 \\
 No shared hits & 
 \end{tabular}
 \caption{The cuts the standard muon tagger applies to select its tagging particles}
 \label{tab:standard-muon-cuts}
\end{table}

\begin{table}[tb]
 \centering
 \begin{tabular}{l}
 \textbf{Variable} \\ 
 \midrule
 Total number of tracks that passed the selection in Table~\ref{tab:preliminary-cuts} \\
 \pt of the signal \B meson candidate \\
 \ptot of the muon tagging candidate \\
 \pt of the muon tagging candidate \\
 \pbox{20cm}{Primary vertex impact parameter of the muon tagging \\ \hphantom{--} candidate divided by its error} \\
 Number of primary vertices in the event \\
 Number of tagging candidates that passed the selection in Table~\ref{tab:standard-muon-cuts}
 \end{tabular}
 \caption{The variables the standard muon tagger uses to calculate its predicted mistag fraction}
 \label{tab:standard-muon-eta-nn}
\end{table}

This note describes a different method of making the tagging particle selection. Rather than making a series of cuts, it uses a neural network which has been trained to select the correct single particle from all those that passed the initial preselection described in Table~\ref{tab:preliminary-cuts}.
