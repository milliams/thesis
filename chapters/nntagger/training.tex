\section{NeuroBayes training}

In order to build a discriminator between the tagging particles we are looking for and those which we will consider background, NeuroBayes\cite{NeuroBayes} was used. NeuroBayes is a neural network package providing many advanced features such as automated preprocessing and decorrelation of input variables. While it has many possible modes of operation, for our purpose it was simply used in the standard classification mode. Given two samples of data (signal and background) and a number of discriminating variables, NeuroBayes will try to provide a single variable which will tend towards $1$ for signal and $-1$ for background.

NeuroBayes was chosen over other MVA or neural network packages such as TMVA due to its ease of use, mostly due to its pre- and postprocessing of variables. It is able to handle input variables which are correlated with one other while extracting the maximum amount of information. This allows all relevant variables to be passed as inputs without having to consider their correlations. It will also automatically remove any variables which provide too little information to the network.

For each tagger, NeuroBayes was used to train a neural network
to select those particles which physically come from the processes discussed.
To train the network, a sample of simulated events was used.
The particles in the simulated events
which were created through the correct physical process
were used as the signal sample and the remaining particles
(after some selections, covered in Section~\ref{sec:training-with-neurobayes})
were used as the background sample.
This network was then used directly in each tagger
to select the most appropriate tagging candidate.
The application of the neural network as a tagger is covered in Section~\ref{sec:testing-on-real-data}.

\subsection{Training sample and event selection}

The NeuroBayes neural network was trained on two separate channels (\decay{\B}{{\D}X} and \decay{\Bp}{\jpsi\Kp}) in order to study the effect that channel-specific training has on the tagger performance.

The neural network was trained on a sample of simulated MC (Monte Carlo) from the MC10 dataset. The stripping stream B2DX with simulation conditions of \unit{3500}{\gev}, magnet both up and down, $\nu=2.5$, processing pass \texttt{Reco08} with \texttt{Stripping12} was used. From inside that stream, the events in the stripping lines listed in Table~\ref{tab:muon-nn-training-stripping-lines} were used. The reason so many stripping lines were used was in order to increase the statistics available for training. Signal candidates were selected using the BackgroundCategory tool which matches the true MC decay tree against the decay channel to only select true signal.

%stripping (number 11960000)

\begin{table}[tb]
 \centering
 \begin{tabular}{l}
 \textbf{Stripping lines} \\
 \midrule
 B2twobodyLine \\
 B2DXWithD2hhhLine \\
 B2DXWithD2hhLine \\
 B2DXWithD2KshhLine \\
 AllB2DDLineLoose \\
 B2Charged2Body \\
 B2DXWithD2KPiPi0MergedLine \\
 B2DXWithD2KPiPi0ResolvedLine \\
 B2DXWithLambdaLine
 \end{tabular}
 \caption{The stripping lines used for training the neural network on \decay{\B}{{\D}X}}
 \label{tab:muon-nn-training-stripping-lines}
\end{table}

The second channel that the neural network was trained on was simulated \decay{\Bp}{\jpsi\Kp} events. Events from stripping line BetaSBu2JpsiKUnbiasedLine from Stripping13 under the same simulation conditions as the previous sample were used with a further selection as detailed in \cite{LHCb-ANA-2011-001}. Both magnet-up and magnet-down data were used and combined into one data set. In the case of multiple candidates in an event, the one with the lowest IP \chisq is chosen.

% \begin{table}[tb]
%  \centering\begin{tabular}{l|l}
%  Variable & Cut \\
%  \midrule
%  \multicolumn{2}{c}{\mumu} \\
%  \midrule
%  \dllmupi & $> 0$ \\
%  Track \chisq per d.o.f. & $< 4$ \\
%  \pt & $> 500$ MeV \\
%  \midrule
%  \multicolumn{2}{c}{\jpsi} \\
%  \midrule
%  End vertex \chisq per d.o.f. & $< 11$ \\
%  Mass & 3016 -- 3176 MeV \\
%  $(\mathrm{Mass} - 3096.916)/\mathrm{error}$ & $< 7$ MeV \\
%  \midrule
%  \multicolumn{2}{c}{\Kp} \\
%  \midrule
%  Track \chisq per d.o.f. & $< 4$ \\
%  \ptot & $> 10000$ MeV \\
%  \pt & $> 1000$ MeV \\
%  \dllkpi & $> 0$ \\
%  $\mathrm{DLL}_{\kaon\proton}$ & $> -2$ \\
%  \midrule
%  \multicolumn{2}{c}{\B Candidate} \\
%  \midrule
%  Mass & 5100 -- 5450 MeV \\
%  End vertex \chisq per d.o.f. & $< 5$ \\
%  Min IP \chisq & $< 25$
%  \end{tabular}
%  \caption{The cuts used to select \decay{\Bp}{\jpsi\Kp}}
%  \label{tab:jpsik-selection-cuts}
% \end{table}

\subsubsection{Opposite-side muon tagger}

Once a signal \B meson candidate is selected, all the particles in the event (excluding those which were used in the reconstruction of the signal candidate) which pass the cuts in Table~\ref{tab:preliminary-cuts} are looped through to select suitable muons for tagging. Only particles which are defined as muons \cite{Lanfranchi:isMuon} are used with an additional requirement that there are no other muons sharing hits in the detector with this particle. For each of the particles passing these cuts, an attempt to find an associated MC particle is made; if none can be found then the particle is not considered further.

%Also muons from secondary decays...?

\subsubsection{Same-side pion tagger}

The neural network for the same-side pion tagger was trained on the same initial \decay{\Bp}{\jpsi\Kp} sample as the muon tagger. It was not trained on the \decay{\B}{{\D}X} sample. Additionally, is it required that only particles from long tracks are considered and that the particles are pion-like ($\dllkpi < 5$). Also, as for the muon tagger, a requirement that the particle has an associated MC particle is applied.

\subsection{Training with NeuroBayes} \label{sec:training-with-neurobayes}

\subsubsection{Opposite-side muon tagger}

For training the neural network, a signal and background sample must be defined. The network was trained to be able to distinguish true opposite-side muons from anything else that may have passed the selections up until now. In order to achieve this, particles which were identified as muons by looking at MC truth while also being a descendant of a \B meson (other than the signal candidate) were used as signal while all other particles were used as background.

No explicit attempt was made to exclude secondary muons (as defined in Section \ref{sec:opposite-side-muon-tagger}) and so for the purpose of the training, they were considered as signal.

Once all the tagging candidate muons had been classified into either signal or background, they were passed into NeuroBayes. The neural network was trained on the parameters in Table~\ref{tab:muon-nn-variables} and using the NeuroBayes settings listed in Table~\ref{tab:neurobayes-training-settings}.

\begin{table}[tb]
 \centering
 \begin{tabulary}{\linewidth}{>{\everypar{\hangindent1.2em}}L}
 \textbf{Training variable} \\
 \midrule
 momentum of the tagging candidate, \ptot \\
 transverse momentum of the tagging candidate, \pt \\
 $P_l$ of the tagging candidate, defined as $\ptot(\B) \cdot \ptot(\mmu)$ \\
 Measured mass of the signal \B meson \\
 Primary vertex impact parameter (PVIP) of the tagging candidate, its error and PVIP/error \\
 Signed PVIP of the tagging candidate \\
 Minimum IP of the tagging candidate w.r.t the other reconstructed primary vertices (IPPU) \\
 \mphi of the momentum of the tagging candidate, $\mphi(\ptot(\mmu))$ \\
 \mtheta of the momentum of the tagging candidate, $\mtheta(\ptot(\mmu))$ \\
 $\delta\eta$, the difference in $\eta$ between the \mmu and the signal \B candidate \\
 $\delta\phi$, defined as $\lvert\phi(\ptot(\mmu)) - \phi(\ptot(\B))\rvert$ \\
 electric charge of the tagging candidate \\
 track likelihood of the tagging candidate \\
 track $\nicefrac{\chi^2}{DoF}$ of the tagging candidate \\
 $\delta Q$, defined as $\lvert\ptot(\B) + \ptot(\mmu)\rvert - \lvert\ptot(\B)\rvert$ \\
 clone distance \\
 \dllmupi of the tagging candidate \\
 \dllkpi of the tagging candidate \\
 Impact parameter with opposite-side \B SV, its error and IPSV/error \\
 Distance of closest-approach (DOCA) to opposite-side \B SV, its error and DOCASV/error
 \end{tabulary}
 \caption[The variables used to train the muon neural network.]{The variables used to train the muon neural network.  A complete description of the variables can be found in \cite{Grabalosa:1456804}.}
 \label{tab:muon-nn-variables}
\end{table}

\begin{table}[tb]
 \centering
 \begin{tabular}{l|l}
 \textbf{NeuroBayes option} & \textbf{value} \\
 \midrule
 Type of task & Binomial classification \\
 Regularisation scheme &  ARD and ASR \\
 Loss function & ENTROPY \\
 Training method & BFGS algorithm \\
 Include diagonal learning term & Yes \\
 \multirow{3}{*}{Input preprocessing} & Keep variables with significance $>3\sigma$ \\
 & Decorrelate input variables \\
 & \pbox{20cm}{Transform input variables to \\ \hphantom{--} Gaussian distributions}
 \end{tabular}
 \caption{The NeuroBayes settings used for training}
 \label{tab:neurobayes-training-settings}
\end{table}

The output of the \decay{\B}{{\D}X} trained neural network when applied to a testing dataset is shown in Fig.~\ref{fig:neurobayes-muon-training-BDX}. There is clear separation between signal and background. Fig.~\ref{fig:neurobayes-muon-training-BDX-ROC} shows the ROC curve of the network. The blue curve is the efficiency of selecting signal plotted against the efficiency of selecting \emph{any} event. The nearer to the top-left corner the blue curve sits, the purer the selection. A straight line from ($0,0$) to ($1,1$) represents random selection. The grey triangle on the left (defined by the point at $(0.18,1.0)$) represents the fact that given a perfect selection you could keep 18\% of the data while keeping 100\% of the signal. That is, 82\% of the data set is background --- the grey areas are defined purely by the ratio of signal to background in the testing data set and are not affected by the quality of the trained network.

% from both rounds of training

\begin{figure}
 \centering
 \includegraphics[width=0.9\textwidth]{\currfiledir/figs/BDX_muon_NN_training.png}
 \caption{NeuroBayes response of the training of the \mmu tagger when trained on the \decay{\B}{{\D}X} channel. Red is signal, black is background.}
 \label{fig:neurobayes-muon-training-BDX}
\end{figure}

\begin{figure}
 \centering
 \includegraphics[width=0.9\textwidth]{\currfiledir/figs/BDX_muon_NN_training_ROC.png}
 \caption{ROC curve showing the efficiency of the \mmu tagger when trained on the \decay{\B}{{\D}X} channel.}
 \label{fig:neurobayes-muon-training-BDX-ROC}
\end{figure}

Fig.~\ref{fig:neurobayes-muon-training-BJPsiK} and \ref{fig:neurobayes-muon-training-BJPsiK-ROC} show the output of a neural network created with the same settings as the previous one except that the training sample came from \decay{\Bp}{\jpsi\Kp} decays.

\begin{figure}
 \centering
 \includegraphics[width=0.9\textwidth]{\currfiledir/figs/BJPsiK_muon_NN_training_def3.png}
 \caption{NeuroBayes response of the training of the \mmu tagger when trained on the \decay{\Bp}{\jpsi\Kp} channel. Red is signal, black is background.}
 \label{fig:neurobayes-muon-training-BJPsiK}
\end{figure}

\begin{figure}
 \centering
 \includegraphics[width=0.9\textwidth]{\currfiledir/figs/BJPsiK_muon_NN_training_def3_ROC.png}
 \caption{ROC curve showing the efficiency of the \mmu tagger when trained on the \decay{\Bp}{\jpsi\Kp} channel.}
 \label{fig:neurobayes-muon-training-BJPsiK-ROC}
\end{figure}

During its preprocessing phase, NeuroBayes prunes out variables which do not contribute much power to the discrimination. Of the 26 variables which were initially used as inputs, only those shown in Table~\ref{tab:muon-nn-significant-variables} were actually used in the final network.

\begin{table}[tb]
 \centering
 \begin{tabular}{l|c}
 \textbf{Training variable} & \textbf{Significance} \\
 \midrule
 \pt & 45 \\
 Signed PVIP & 21 \\
 \dllmupi & 16 \\
 Track \chisq per d.o.f. & 9.5 \\
 PVIP / error & 8.8 \\
 \dllkpi & 7.6 \\
 \ptot & 5.2 \\
 IPPU & 3.5
 \end{tabular}
 \begin{tabular}{l|c}
 \textbf{Training variable} & \textbf{Significance} \\
 \midrule
 \pt & 171 \\
 Signed P & 76 \\
 \dllmupi & 57 \\
 Track \chisq per d.o.f. & 39 \\
 \dllkpi & 34 \\
 PVIP / error & 30 \\
 \ptot & 17 \\
 IP with opposite-side SV & 11 \\
 IPPU & 10 \\
 Error on IP with opposite-side SV & 9.6 \\
 DOCA to opposite-side SV & 7.0 \\
 Track likelihood & 5.8
 \end{tabular}
 \caption{The training variables remaining after NeuroBayes pruned those with too low a significance. On the left is the training based on the \decay{\B}{{\D}X} sample. The \decay{\Bp}{\jpsi\Kp} is shown on the right.}
 \label{tab:muon-nn-significant-variables}
\end{table}

\subsubsection{Same-side pion tagger}

For training the neural network a choice of signal and background definition must be made, particularly which types of same-side pions are included in the signal sample. Initially the network was trained on a signal sample consisting of both pions coming from a $B^{**}$ and those which came from associated production (by requiring MC truth). The neural network was trained on the parameters in Table~\ref{tab:pion-nn-variables}. The neural network output can be seen in Fig.~\ref{fig:pion-nn-training-2and3} and the ROC curve in Fig.~\ref{fig:pion-nn-training-2and3-ROC}.

\begin{table}[tb]
 \centering
 \begin{tabular}{l}
 \textbf{Training variable} \\
 \midrule
 \ptot \\
 \pt \\
 $p_l$, defined as $\ptot(\B) \cdot \ptot(\pion)$ \\
 PVIP and its error \\
 Signed PVIP \\
 $\delta R$, defined as $\sqrt{\delta\eta^2+\delta\phi^2}$ \\
 \mphi of the momentum of the tagging candidate, $\mphi(\ptot(\pion))$ \\
 \mtheta of the momentum of the tagging candidate, $\mtheta(\ptot(\pion))$ \\
 track likelihood \\
 track $\nicefrac{\chi^2}{DoF}$ \\
 $\delta Q$, defined as $\lvert\ptot(\B) + \ptot(\pion)\rvert - \lvert\ptot(\B)\rvert$ \\
 \dllkpi \\
 \pbox{20cm}{Invariant mass of the tagging pion and signal \\ \hphantom{--} \B meson candidate combination}
 \end{tabular}
 \caption[The variables used to train the pion neural network.]{The variables used to train the pion neural network. A complete description of the variables can be found in \cite{Grabalosa:1456804}.}
 \label{tab:pion-nn-variables}
\end{table}

The fact that the signal and background peaks are almost directly on top on one another shows that the neural network training is largely unable to differentiate between the two samples. A possible solution to this is to define only one of the two type of pion as signal and allow the other to be treated as background. 

\begin{figure}
 \centering
 \includegraphics[width=0.9\textwidth]{\currfiledir/figs/BJPsiK_pion_NN_training_def2ish.png}
 \caption{The training output for the same-side pion neural network when both $B^{**}$ and pions from associated production are used as the signal sample.}
 \label{fig:pion-nn-training-2and3}
\end{figure}

\begin{figure}
 \centering
 \includegraphics[width=0.9\textwidth]{\currfiledir/figs/BJPsiK_pion_NN_training_def2ish_ROC.png}
 \caption{ROC curve showing the efficiency of the same-side pion tagger when trained on a sample treating both $B^{**}$ and associated production pions as signal.}
 \label{fig:pion-nn-training-2and3-ROC}
\end{figure}

The network was then trained using only the $B^{**}$ pions as signal and including pions from associated production as background. The network output for this can be seen in Fig.~\ref{fig:pion-nn-training-just2}. It is clear here that the network training was easily able to separate signal and background with the majority of the background being pushed to $-1$. The ROC curve for this training is shown in Fig.~\ref{fig:pion-nn-training-just2-ROC} and it is clearly an improvement over Fig.~\ref{fig:pion-nn-training-2and3-ROC}.

\begin{figure}
 \centering
 \includegraphics[width=0.9\textwidth]{\currfiledir/figs/BJPsiK_pion_NN_training_def5.png}
 \caption{The training output for the same-side pion neural network when using just $B^{**}$ pions as the signal sample.}
 \label{fig:pion-nn-training-just2}
\end{figure}

\begin{figure}
 \centering
 \includegraphics[width=0.9\textwidth]{\currfiledir/figs/BJPsiK_pion_NN_training_def5_ROC.png}
 \caption{ROC curve showing the efficiency of the same-side pion tagger when trained on a sample treating only $B^{**}$ pions as signal.}
 \label{fig:pion-nn-training-just2-ROC}
\end{figure}

Like with the muon tagger training, NeuroBayes prunes out variables which do not contribute much power to the network during its preprocessing phase. Table~\ref{tab:pion-nn-significant-variables} shows which of the variables were used by NeuroBayes in the final network.

\begin{table}[tb]
 \centering
 \begin{tabular}{l|c}
 \textbf{Training variable} & \textbf{Significance} \\
 \midrule
 $p_l$ & 166 \\
 $\delta R$ & 90 \\
 Signed PVIP & 53 \\
 $\theta$ & 24 \\
 track $\nicefrac{\chi^2}{DoF}$ & 14 \\
 \ptot & 12 \\
 \pt & 8.0 \\
 $\mphi(\ptot(\mmu))$ & 6.0 \\
 \dllkpi & 4.3 \\
 \pbox{20cm}{Invariant mass of the tagging pion and  signal \\ \hphantom{--} \B meson candidate combination} & 3.5 \\
 Error on the PVIP & 3.4
 \end{tabular}
 \caption{The training variables remaining after NeuroBayes pruned those with too low a significance}
 \label{tab:pion-nn-significant-variables}
\end{table}

The final network used for the same-side pion tagging algorithm is the one using $B^{**}$ as the signal sample.
