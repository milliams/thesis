%\section{Testing on Real Data \decay{\B}{{\D}X}}

\section[Testing on real data \texorpdfstring{\decay{\Bp}{\jpsi\Kp}}{B⁺→J/ψK⁺}]{Testing on real data \texorpdfstring{\boldmath\decay{\Bp}{\jpsi\Kp}}{B⁺→J/ψK⁺}} \label{sec:testing-on-real-data}

To get a real idea of how well the tagger is performing, it must be applied to real data. This will ensure that the tagger is not just depending on any details of the MC which may not be well representing the data. In order to test a tagger on real data, the easiest way is to use a self-tagging channel --- that is a channel which has a charged \B meson (which will not oscillate). Using such a channel, the charge of the \B meson can be reconstructed without using any flavour tagging algorithms. The flavour tagging is then run blindly, as if this decay was a real signal channel and these results can be compared.

\subsection{Calibrating the mistag fraction}

As well as returning a prediction of the flavour of the signal \B meson, the tagger also has to return an estimate of the mistag probability. That is, how likely it is that the predicted tag is incorrect. The current taggers each use a special neural network which is trained to return an estimate of the mistag probability. This estimate is then calibrated on certain channels in order to give a more accurate reading of the mistag probability.

For the new neural network-based taggers, the output of the neural network can be used directly as a first estimate of the mistag probability since the network has been trained to assign good tagging particles to a value of $N_{out} = 1$ and particles which are not the one being looked for to a value of $N_{out} = -1$. By remapping this range to a pseudo-mistag probability with \begin{equation}\eta_N = \frac{-1 \times N_{out}}{4} + 0.25\end{equation} we get a distribution of pseudo-mistag probabilities ranging from $0.0$ (very good probability of being correct) to $0.5$ (very low probability of being correct).

For each event, the single particle with the lowest value of $\eta_N$ is chosen and the properties of that particle (tag decision and $\eta_N$) are used as the values for the event.

All tagged events were divided into bins of $\eta_N$ and for each bin the fraction of events for which the tagger gave an incorrect answer ($\omega$) for was calculated. Plotting and fitting this binned distribution allows a calibration function between $\eta_N$ and $\omega$ to be found. Fitting a second order polynomial against the data gives the calibration coefficients $p_0$, $p_1$ and $p_2$ of the equation \begin{equation}\omega(\eta) = p_0 + p_1\eta_N + p_2\eta_N^2.\end{equation}

This process is carried out for each tagger/channel combination being studied.

\subsubsection[Opposite-side muon tagger trained on \texorpdfstring{\boldmath\decay{\B}{{\D}X}}{B→DX}]{Opposite-side muon tagger trained on \texorpdfstring{\decay{\B}{{\D}X}}{B→DX}}

Fig.~\ref{fig:muon-nn-BDX-mistag-calibration} shows the mistag fraction in bins of $\eta_N$. Fig.~\ref{fig:muon-nn-BDX-calibrated-eta-distribution} shows the distribution of events after calibration.

\begin{figure}
 \centering
 \includegraphics[width=0.9\textwidth]{\currfiledir/figs/calibration_muon_NN_BDX.png}
 \caption{Calibration of the \decay{\B}{{\D}X}-trained $\mu$ NN mistag probability on data. The fitted calibration curve is plotted on top.}
 \label{fig:muon-nn-BDX-mistag-calibration}
\end{figure}

\begin{figure}
 \centering
 \includegraphics[width=0.9\textwidth]{\currfiledir/figs/calibrated_eta_muon_BDX_data.png}
 \caption{Distribution of mistag of events after calibration for the \decay{\B}{{\D}X}-trained \mmu tagger. The black data point are the neural network tagger and the red points are the existing tagger.}
 \label{fig:muon-nn-BDX-calibrated-eta-distribution}
\end{figure}

From this fit we obtain
\begin{align*}
p_0 &= 0.152 \pm 0.038, \\
p_1 &= 0.94 \pm 0.34, \\
p_2 &= -0.51 \pm 0.58.
\end{align*}

\subsubsection[Opposite-side muon tagger trained on \texorpdfstring{\decay{\Bp}{\jpsi\Kp}}{B⁺→J/ψK⁺}]{Opposite-side muon tagger trained on \texorpdfstring{\boldmath\decay{\Bp}{\jpsi\Kp}}{B⁺→J/ψK⁺}}

Fig.~\ref{fig:muon-nn-BJPsiK-mistag-calibration} shows the mistag fraction in bins of $\eta_N$. Fig.~\ref{fig:muon-nn-BJPsiK-calibrated-eta-distribution} shows the distribution of events after calibration.

\begin{figure}
 \centering
 \includegraphics[width=0.9\textwidth]{\currfiledir/figs/calibration_muon_NN.png}
 \caption{Calibration of the \decay{\Bp}{\jpsi\Kp}-trained $\mu$ NN mistag probability on data. The fitted calibration curve is plotted on top.}
 \label{fig:muon-nn-BJPsiK-mistag-calibration}
\end{figure}

\begin{figure}
 \centering
 \includegraphics[width=0.9\textwidth]{\currfiledir/figs/calibrated_eta_muon_data.png}
 \caption{Distribution of mistag of events after calibration for the \decay{\Bp}{\jpsi\Kp}-trained \mmu tagger. The black data points are the neural network tagger and the red points are the existing tagger.}
 \label{fig:muon-nn-BJPsiK-calibrated-eta-distribution}
\end{figure}

From this fit we obtain
\begin{align*}
p_0 &= 0.108 \pm 0.040, \\
p_1 &= 1.26 \pm 0.36, \\
p_2 &= -0.98 \pm 0.61.
\end{align*}

\subsubsection[Same-side pion tagger trained on \texorpdfstring{\decay{\Bp}{\jpsi\Kp}}{B⁺→J/ψK⁺}]{Same-side pion tagger trained on \texorpdfstring{\boldmath\decay{\Bp}{\jpsi\Kp}}{B⁺→J/ψK⁺}}

Fig.~\ref{fig:pion-nn-mistag-calibration} shows the mistag fraction in bins of $\eta$. Fig.~\ref{fig:pion-nn-calibrated-eta-distribution} shows the distribution of events after calibration.

\begin{figure}
 \centering
 \includegraphics[width=0.9\textwidth]{\currfiledir/figs/calibration_pion_NN.png}
 \caption{Calibration of the $\pion$ NN mistag probability. The fitted calibration curve is plotted on top.}
 \label{fig:pion-nn-mistag-calibration}
\end{figure}

\begin{figure}
 \centering
 \includegraphics[width=0.9\textwidth]{\currfiledir/figs/calibrated_eta_pion_data.png}
 \caption{Distribution of mistag of events after calibration for the SS\pion tagger. The black data point are the neural network tagger and the red points are the existing tagger.}
 \label{fig:pion-nn-calibrated-eta-distribution}
\end{figure}

From this fit we obtain
\begin{align*}
p_0 &= 0.13 \pm 0.19, \\
p_1 &= 1.4 \pm 1.1, \\
p_2 &= -1.4 \pm 1.4.
\end{align*}

\subsection{Measuring the tagging power}

The effective tagging efficiency is defined as
\begin{equation}
 \effeff = \etag D^2,
\end{equation}
where $\etag$ is the efficiency with which the tagger returns a result and $D$ is the dilution, defined as
\begin{equation}
 D = 1-2\omega.
\end{equation}

\begin{align}
\langle D^2 \rangle &= \int p(\eta) D(\eta,p_0,p_1,p_2)^2 \mathrm{d}\eta \\
&= \frac{1}{N} \sum_i h(\eta_i) D(\eta,p_0,p_1,p_2)^2,
\end{align}
where $p(\eta)$ is the p.d.f of $\eta$, $h(\eta_i)$ is the binned version of the same data and $N = \sum_i h(\eta_i)$. So summing over the bins of $\eta$ multiplied by the calibrated squared dilution of the events in that bin gives the expected overall squared dilution.

The errors on the tagging power are calculated in the same manner as described in \cite[Appendix~H.4]{LHCb-ANA-2011-36}

% \begin{align}
% \sigma^2( D^2 ) = {} &
% \sigma^2(p_0) \left(\frac{\ud D^2}{\ud p_0}\right)^2 \; + \;
% \sigma^2(p_1) \left(\frac{\ud D^2}{\ud p_1}\right)^2 \nonumber \\
% & \; + \; 2 \rho(p_0,p_1)\:\sigma(p_0)\:\sigma(p_1) \:
% \left(\frac{\ud D^2}{\ud p_0}\right)\:
% \left(\frac{\ud D^2}{\ud p_1}\right)
% \end{align}
% 
% where
% 
% \begin{equation}
%   \frac{\ud D^2}{\ud p_0} \; = \;
%   \frac{1}{N} \sum 2 D_i \frac{\ud D_i}{\ud p_0} \; = \;
%   \frac{1}{N}\sum -4 D_i
% \end{equation}
% \begin{equation}
%   \frac{\ud D^2}{\ud p_1} \; = \;
%   \frac{1}{N} \sum 2 D_i \frac{\ud D_i}{\ud p_1} \; = \;
%   \frac{1}{N}\sum -4 ( \eta_i - \langle \eta \rangle ) D_i
% \end{equation}

\subsubsection{Opposite-side muon tagger}

The results for the \mmu tagger are shown in Table~\ref{tab:muon-nn-results}. All three results were evaluated on the same set of \decay{\Bp}{\jpsi\Kp} data. \emph{Standard} is the tagger present in v12r5 of the \emph{FlavourTagging} package. The two neural network results differ only by the sample used to train the network.

\begin{table}[tb]
 \centering
 \begin{tabular}{r|SSS}
 {\textbf{Tagger}} & {\etag (\%)} & {\mistag (\%)} & {\effeff (\%)} \\
 \midrule
 Standard & 5.15 & 29.3 \pm 1.9 & 0.88 \pm 0.11 \\
 \decay{\B}{{\D}X} Neural Network & 12.44 & 36.0 \pm 1.2 & 1.00 \pm 0.12 \\
 \decay{\Bp}{\jpsi\Kp} Neural Network & 9.91 & 34.0 \pm 1.2 & 0.99 \pm 0.10
 \end{tabular}
 \caption{The results of the neural network-based tagger compared to the existing OS$\mmu$ tagger}
 \label{tab:muon-nn-results}
\end{table}

\subsubsection{Same-side pion tagger}

The SS\pion tagger's performance is shown in Table~\ref{tab:pion-nn-results}. This neural network was both trained (MC) and tested (real data) on the \decay{\Bp}{\jpsi\Kp} channel.

\begin{table}[tb]
 \centering
 \begin{tabular}{r|SSS}
 {\textbf{Tagger}} & {\etag (\%)} & {\mistag (\%)} & {\effeff (\%)} \\ 
 \midrule
 Standard & 10.34 & 39.4 \pm 1.2 & 0.47 \pm 0.08 \\
 Neural Network & 89.87 & 46.0 \pm 1.0 & 0.58 \pm 0.09
 \end{tabular}
 \caption{The results of the neural network-based tagger compared to the existing SS$\pion$ tagger}
 \label{tab:pion-nn-results}
\end{table}

\subsection{Conclusions}

By changing the implementation of the flavour tagger from a simple cut-based model to one using a neural network classifier the performance of the muon tagger shows a clear $_{\widetilde{}}\,10\%$ improvement in overall tagging power. While this is at the expense of average wrong tag fraction, the increased overall efficiency and the distribution of tagged events (Fig.~\ref{fig:muon-nn-BJPsiK-calibrated-eta-distribution}) give an improved event-by-event tagging performance.

In terms of tagging performance, there is little difference between the performance of the OS\mmu tagger when trained on either of the two training channels. This indicates that the trained network shown here should work on most channels without having to retrain.

The SS\pion tagger shows an overall improvement in event-by-event tagging power but at the expense of average mistag fraction. The high \etag is due to the network always being able to find at least one appropriate pion in the event, even if it is not the correct one for tagging. The average mistag fraction could be reduced with a network which can accurately classify associated production pions as well as $B^{**}$ pions which the network shown here was unable to do.

\subsubsection{Implementation}

The neural network based \mmu tagger is available in the FlavourTaggingChecker package.
