\chapter{Introduction} \label{chapter:dsk_introduction}

\newcommand{\rdsk} {\ensuremath{r_{\Ds K}}\xspace}
\newcommand{\gammabeta} {\ensuremath{\gamma - 2 \beta_s}\xspace}

I was involved with the measurement of the time-dependent \CP-violation observables in \BsDsK decays.
While that analysis focused on \BsDsK, it included \BsDspi as a cross-check channel and so a full event selection, mass fit and systematic uncertainty study was performed for that channel.
Since much of the work performed for the \BsDsK study feeds directly into the main analysis topic presented in this thesis
I will here give an overview of the analysis, particularly as it pertains to the main analysis on \BsDspi.
A full internal analysis note was written up as Ref~\cite{LHCb-ANA-2012-068} with further details and was submitted as a conference paper as Ref~\cite{LHCb-CONF-2012-029}.

The purpose of the analysis is to measure \Cpar, \Spar, \Sbpar, \Dpar and \Dbpar
on \BsDsK decays from 2011 data from \lhcb over a dataset of integrated luminosity $\intlum{1.0 \,\invfb}$ of $\proton\proton$ collisions recorded at a centre-of-mass energy of $\sqrt{s}=\unit{7}{\tev}$.
These \CP parameters are related to the physics parameters \rdsk, $\Delta$ and \gammabeta by
\begin{align}
 C &= \frac{1-\rdsk^2}{1+\rdsk^2}, \label{eq:dsk_phys_params_C} \displaybreak[0] \\
 \Dpar &= \frac{2 \rdsk \cos(\Delta - (\gammabeta))}{1+\rdsk^2}, \label{eq:dsk_phys_params_Dpar} \displaybreak[0] \\
 \Dbpar &= \frac{2 \rdsk \cos(\Delta + (\gammabeta))}{1+\rdsk^2}, \label{eq:dsk_phys_params_Dbpar} \displaybreak[0] \\
 \Spar &= \frac{2 \rdsk \sin(\Delta - (\gammabeta))}{1+\rdsk^2}, \label{eq:dsk_phys_params_Spar} \displaybreak[0] \\
 \Sbpar &= \frac{2 \rdsk \sin(\Delta + (\gammabeta))}{1+\rdsk^2}, \label{eq:dsk_phys_params_Sbpar}
\end{align}
where $\rdsk = \left| A(\decay{\Bsb}{\Dsm\Kp}) / A(\decay{\Bs}{\Dsm\Kp}) \right|$ is the ratio of the magnitudes of the decay amplitudes and $\Delta$ is the strong phase difference.
The \Bs mixing phase, $\beta_s$ is predicted by the Standard Model to be small and so from this it is possible to constrain the CKM angle $\gamma$.

\input{chapters/dsk/selection}

% \chapter{Data selection} \label{chapter:dsk_selection}
% 
% The reconstruction of the \BsDsK signal events was performed in the same way as for the \BsDspi analysis described in Chapter~\ref{chapter:data_selection}.
% The only difference is that rather than using a \pion mass hypothesis when combining the tracks, a kaon mass hypothesis is used.
% 
% Once the \Bs candidates are available, the selection is optimised to select \BsDsK decays as described in Section~\ref{sec:offline_selection}.
% The BDTG requirement is tuned on \BsDsK candidates and is found that for optimal selection the response must be greater than $0.5$.
% 
% The bachelor kaon is selected using particle identification requirements.
% As discussed in Section~\ref{sec:pid}, the main discriminating variable is \dllkpi which is defined as the logarithm of the likelihood of having a certain detector response given the hypothesis that the particle is a kaon minus the same given the hypothesis of it being a pion.
% It would be preferable to perform a significance scan across a range of \dllkpi (perhaps a range of $-10$ to $20$)
% but since changing the particle identification requirement would require recomputing the shapes of the backgrounds used in the mass fit, only two values of \dllkpi are tested.
% Testing at $\dllkpi>5$ and $\dllkpi>10$ gives significances of the signal yield of $29.5\sigma$ and $30.5\sigma$ respectively.
% More importantly, the tighter requirement greatly reduces the cross-feed contribution of \BsDspi under the signal peak.
% Therefore, a particle identification requirement of $\dllkpi>10$ is applied.
% 
% After the BDTG and particle identification requirements are applied, the distributions of reconstructed \Bs mass and decay-time are shown in Figure~\ref{fig:offline_dsk2}.
% 
% \begin{figure}[htb]
%   \centering
%   \includegraphics[width=.49\textwidth]{\currfiledir/Offline_Bs2DK_merged_mass.pdf}
%   \includegraphics[width=.49\textwidth]{\currfiledir/Offline_Bs2DK_merged_ctau.pdf}
%   \caption{Distributions of key quantities \BsDsK data after the offline selection was applied.}
%   \label{fig:offline_dsk2}
% \end{figure}

\input{chapters/tagging/tagging}

%\input{chapters/dsk/backgrounds}

\chapter{Mass fit} \label{chapter:dsk_mass_fit}

In order to extract yields for the background contributions in the decay-time fit, a fit is performed on the distribution of reconstructed \Bs mass.
The most important part of this is understanding the shapes of the backgrounds and the signal.

The signal is described with a double Crystal Ball function with tails pointing in opposite directions.
The shapes of these functions are fixed using a fit to simulated data which has had the full selection applied to it.
All shape parameters are floated freely in the fit and the results are given in Table~\ref{tab:dsk_doubleCB}.
The resultant PDF and the simulated data it was fitted to are shown in Figure~\ref{fig:dsk_signal_lineshape}.
In the final mass fit to data, the tail parameters remain fixed but the widths and means are floated.

\begin{table}[htb]
 \centering
 \begin{tabular}{cc}
  \toprule
  \textbf{Parameter}  &  \textbf{Fitted value}  \\
  \midrule
  $\mu_{\rm DOWN}$    &  5366.5  $\pm$ 0.09 \mevcc \\
  $\mu_{\rm UP}$      &  5366.6  $\pm$ 0.09 \mevcc  \\
  $\sigma_1$          &  10.88   $\pm$ 0.11 \mevcc  \\ 
  $\sigma_2$          &  15.71   $\pm$ 0.09 \mevcc  \\
  ${\alpha}_1$        &  1.81   $\pm$ 0.01  \mevcc   \\
  ${\alpha}_2$        &  -1.82  $\pm$ 0.03  \mevcc  \\
  $n_1$               &  1.38   $\pm$ 0.02    \\
  $n_2$               &  8.86    $\pm$ 0.09   \\
  $f$                 &  0.47   $\pm$ 0.01  \\
  \bottomrule
 \end{tabular}
 \caption{Parameters for the sum of the two Crystal Ball functions describing the signal
          shapes of \BsDsK, obtained from simulated data.}
 \label{tab:dsk_doubleCB}
\end{table}

\begin{figure}
  \centering
  \includegraphics[width=\textwidth]{\currfiledir/Signal_Bs2DsK_both.pdf}
  \caption[Signal mass shapes of \BsDspi evaluated on simulated data.]{Signal mass shapes of \BsDsK evaluated on simulated data.
           The solid lines correspond to the fit to the double Crystal Ball function,
           while the dashed lines correspond to the individual Crystal Ball components.
           The bottom plot show the deviation of the data from the fit line based on statistical uncertainty.}
  \label{fig:dsk_signal_lineshape}
\end{figure}

The first background to consider is the combinatorial background.
This was modelled as an exponential function with slope parameters fitted to data in the range $m(\Ds)=(1868,1948)\cup(1990,2068) \mevcc$
and $m(\Bs) > \unit{5600}{\mevcc}$ and then used in the final fit over the full mass range.
The fitted slope parameters for each of the \Ds modes are given in Table~\ref{tab:combinatorial_slopes_dsk}.

\begin{table}[htb]
  \centering
  \begin{tabular}{cS}
  \toprule
  \textbf{\Ds mode}  &  \textbf{Slope parameter ($c^2/\mev$)} \\
  \midrule
  \DsKKpi            &  -1.58  $\pm$ 0.19 \\
  \DsKpipi           &  -1.09  $\pm$ 0.25 \\
  \Dspipipi          &  -0.99  $\pm$ 0.21 \\
  \bottomrule
  \end{tabular}
  \caption{The fitted values (in units of $10^{-3}$) of the slope parameter of an exponential function describing the combinatorial background.}
  \label{tab:combinatorial_slopes_dsk}
\end{table}

Further to this, there are a number of fully- and partially-reconstructed backgrounds.
All these backgrounds are modelled using simulated data, forced to decay to the particular final state.
The two fully-reconstructed backgrounds that factor in the fit are \BsDspi and \BdDsK
where the former is fitted with a sum of Gaussian kernels and the latter is fitted with a double Crystal Ball function.
There are a large number of partially-reconstructed backgrounds: \BdDK, \BsDsKst, \BsDsstK, \BsDsstKst, \BsDsrho, \BsDsstrho, \BsDsstpi, \LbDsp, \LbDsstp and \LbLcK.
The shapes for each of these is taken from simulated data and corrected for the distortion caused by the momentum-dependent particle identification efficiency.
Each background distribution is fitted with a sum of Gaussian kernels.

The yields of \BdDK, \LbDsp, \LbDsstp and \LbLcK are fixed in the mass fit.
The \BdDK yield is fixed based on the yield of \BdDpi calculated in Section~\ref{sec:fully-reconstructed-backgrounds} scaled by a factor $1/15$ based on the relative branching fractions of \BsDspi and \BsDsK \cite{LHCB-PAPER-2011-022} and correcting for the relative particle identification efficiencies of the two $\dllkpi<0$ and $\dllkpi>10$ requirements.
Again for \LbLcK, the yield is based on the fitted yield from the fit to \BsDspi and scaled by the same factor $1/15$ as for \BdDK.
For the final two modes, a sample of events is created where the mass hypothesis used for the bachelor is changed from that of a kaon to a proton.
The sample is then fitted to extract the shapes of the \Lb backgrounds.

The remaining backgrounds are collected into two groups:
\begin{description}
\item[Group 1:]
\BdDsK, \BsDsKst, \BsDsstK, \BsDsstKst
\item[Group 2:]
\BsDspi, \BsDsstpi, \BsDsrho, \BsDsstrho
\end{description}
Each group is combined into a single PDF, defined as
\begin{align}
f_{11} \pdf_{\BdDsK}
&+ ( 1 - f_{11} )[ f_{12} \pdf_{\BsDsKst} \nonumber\\
&+ ( 1 - f_{12}) ( f_{13} \pdf_{\BsDsstK} \nonumber\\
&+ ( 1 - f_{13}) \pdf_{\BsDsstKst} ) ]
\label{eq:group1_bsdsk}
\end{align}
for group 1 and
\begin{align}
f_{21} \pdf_{\BsDspi}
&+  f_{22} \pdf_{\BsDsstpi} \nonumber\\
&+  f_{23} \pdf_{\BsDsrho} \nonumber\\
&+ (1-f_{21}-f_{22}-f_{23})\pdf_{\BsDsstrho}
\label{eq:group2_bsdsk}
\end{align}
for group 2, where $\pdf_{\mathrm{mode}}$ is the PDF for a given mode calculated previously and $f_{NM}$ are relative yield fractions.
The fractions, $f_{2N}$, for group 2 are fixed based on the yields extracted from the mass fit to \BsDspi and scaled appropriately based on the particle identification efficiency.
The values obtained are $f_{21} = 0.428$, $f_{22}=0.472$, and $f_{23}=0.052$.

The results of the final fit are plotted in Figure~\ref{fig:fit-BsDsK2} and the signal shape parameters and event yields are given in Table~\ref{tab:fitmassBsDsK}.

\begin{table}[htb]
  \centering
  \begin{tabular}{lS}
  \toprule
\textbf{Parameter}                &  \textbf{Fitted value}              \\
\midrule
Mean                     &  5370.30 \pm  0.47  \mevcc    \\
$\sigma_{1}$             &  17.43   \pm  1.70  \mevcc    \\
$\sigma_{2}$             &  12.34   \pm  0.86  \mevcc    \\
$f_{11}$                 &  0.206   \pm  0.022    \\
$f_{12}$                 &  0.374   \pm  0.190     \\
$f_{13}$                 &  0.744   \pm  0.106     \\
\midrule
\DsKKpi magnet up        &                           \\
  $\quad$ $N_{\BsDsK}$   &  430      \pm  25        \\
  $\quad$ $N_{Comb}$     &  303      \pm  35        \\
  $\quad$ $N_{Group1}$   &  844      \pm  37        \\
  $\quad$ $N_{Group2}$   &  51       \pm  36        \\
\midrule
\DsKKpi magnet down      &                          \\
  $\quad$ $N_{\BsDsK}$   &  626      \pm  30        \\
  $\quad$ $N_{Comb}$     &  473      \pm  44        \\
  $\quad$ $N_{Group1}$   &  1024     \pm  45        \\
  $\quad$ $N_{Group2}$   &  100      \pm  47        \\
\midrule
\DsKpipi magnet up       &                          \\
  $\quad$ $N_{\BsDsK}$   &  51       \pm  8        \\
  $\quad$ $N_{Comb}$     &  83       \pm  17        \\
  $\quad$ $N_{Group1}$   &  69       \pm  13        \\
  $\quad$ $N_{Group2}$   &  4        \pm  20        \\
\midrule
\DsKpipi magnet down     &                          \\
  $\quad$ $N_{\BsDsK}$   &  47       \pm  8        \\
  $\quad$ $N_{Comb}$     &  115      \pm  20        \\
  $\quad$ $N_{Group1}$   &  90       \pm  15        \\
  $\quad$ $N_{Group2}$   &  21       \pm  17        \\
\midrule
\Dspipipi magnet up      &                          \\
  $\quad$ $N_{\BsDsK}$   &  101      \pm  13        \\
  $\quad$ $N_{Comb}$     &  105      \pm  18        \\
  $\quad$ $N_{Group1}$   &  184      \pm  17        \\
  $\quad$ $N_{Group2}$   &  5        \pm  14        \\
\midrule
\Dspipipi magnet down    &                           \\
  $\quad$ $N_{\BsDsK}$   &  135      \pm  14        \\
  $\quad$ $N_{Comb}$     &  159      \pm  24        \\
  $\quad$ $N_{Group1}$   &  185      \pm  21        \\
  $\quad$ $N_{Group2}$   &  61       \pm  24        \\
  \bottomrule
  \end{tabular}
  \caption[Fitted values of parameters for \BsDsK signal mass fit.]{Fitted values of parameters for \BsDsK signal mass fit. The $N_{i}$ are the yield of the signal and background contributions.
    Mean and sigmas are the parameters of double Crystal Ball function used to describe the signal. The parameters $f_i$ are fractions between
    modes in Group 1 backgrounds: \BdDsK, \BsDsKst, \BsDsstK and \BsDsstKst.}
  \label{tab:fitmassBsDsK}
\end{table}

\begin{figure}[htb]
 \centering
 \includegraphics[width=\textwidth]{\currfiledir/mass_BsDsK_both_all.pdf}
 \caption{Result of the simultaneous mass fit to the \BsDsK candidates.
          The pull distributions are shown in the lower part of the figure.}
 \label{fig:fit-BsDsK2}
\end{figure}

\chapter{Decay-time fit} \label{chapter:dsk_time_fit}\label{sec:sfit}

A conventional fit, such as the one used to fit the mass distribution uses a series of model PDFs for each expected signal and background component.
They are combined together and fitted to the data using a maximum-likelihood method.
However, recently an alternative method of performing time fits has started to become more common.
It is referred to as the \emph{sFit} and is based on the method given in Ref~\cite{sFit}.
The first step of the process is identical to the conventional method discussed so far (which in contrast is referred to as the \emph{cFit}).
A fit to the mass distribution (with full descriptions of the backgrounds) is performed and two PDFs are extracted:
one for the signal and another for the combination of the backgrounds.
From this, a signal \emph{sWeight} is calculated for each event, given by
\begin{equation}
 W_s(y) = \frac{V_{ss} F_s(y) + V_{sb} F_b(y)}{N_s F_s(y) + N_b F_b(y)},
\end{equation}
where $y$ is the variable over which the distribution is being fitted (in this case the \Bs reconstructed mass),
$N_s$ and $N_b$ are the number of signal and background events,
$F_s(y)$ and $F_b(y)$ are the distributions of $y$ for the signal and background respectively
and the matrix $V$ is given by inverting
\begin{equation}
 V_{ij}^{-1} = \sum_{e=1}^N \frac{F_i(y_e) F_j(y_e)}{(N_s F_s(y_e) + N_b F_b(y_e))^2}.
\end{equation}
Using these weights, the time distribution of the events is plotted but with each event weighted by its signal weight.
The resultant distribution gives the time distribution of the signal mode, having effectively removed the background contribution.
This allows a simple fit to be performed on the time distribution without having to model the backgrounds.
A full conventional fit is also performed as a cross-check during the development of the sFit but is not used to produce final results.

Due to the nature of the fit, it is not necessary to parameterise the shapes of the backgrounds since they will have been removed from the distribution.
As such, the fit model is simply a signal decay function given in Equation~\ref{eq:decay_rates} convolved with a resolution function and multiplied by an acceptance function.

\section{Decay-time resolution}

Any measurable oscillation is diluted by the finite decay-time resolution of the detector as well as the measurement being potentially biased.
As such, it is important to account for the time resolution accurately.
The time resolution is modelled by the sum of three Gaussian functions with a common mean, $\mu$, but differing widths, $\sigma_i$, and is parameterised as
\begin{equation}
 R(t) = f_1 G(t;\mu,\sigma_1) + f_2 G(t;\mu,\sigma_2) + (1-f_1-f_2) G(t;\mu,\sigma_3),
 \label{eq:time_resolution_model}
\end{equation}
where $f_i$ are the relative fractions of the first two components.
The parameters for the model are calculated by fitting a sample of simulated \decay{\Bs}{\Ds\pion} events giving the parameters shown in Table~\ref{tab:time_resolution_model}.

\begin{table}
 \centering
 \begin{tabular}{cc}
  \toprule
  \textbf{Parameter}  & \textbf{Value}                \\
  \midrule
  $\sigma_1$          & \unit{29.48\pm0.027}{\invfs}  \\
  $\sigma_2$          & \unit{58.64\pm0.063}{\invfs}  \\
  $\sigma_3$          & \unit{181.7\pm4.9}{\invfs}    \\
  $\mu$               & \unit{1.49\pm0.14}{\invfs}    \\
  $f_1$               & $0.595\pm0.011$               \\
  $f_2$               & $0.386\pm0.011$               \\
  \bottomrule
 \end{tabular}
 \caption{Fitted parameters for the time resolution model.} \label{tab:time_resolution_model}
\end{table}

\section{Decay-time acceptance} \label{sec:decay-time-acceptance}

In addition to a finite time resolution, the events are also selected with a non-uniform acceptance which is a function of decay time.
The \lhcb triggers preferentially ignore events with a short decay time to avoid selecting prompt charm hadrons.
The rate at which these short-lived particles are excluded can be parameterised by an envelope function by which the overall PDF is multiplied.
In addition to the deficit of short-lived particles, there is also an observed increasing reduction in particles as their decay-time increases.
In order to model these features, the following power-law equation is used:
\begin{equation}
 a_{trig}(t) = \left\{
  \begin{array}{ll}
   0 & \text{when $(at)^n - b < 0$ or $t < 0.2$ ps}, \\
   \big(1 - \frac{1}{1+(at)^n - b}\big) \times (1 - \beta t) & \text{otherwise},
  \end{array}
 \right.
 \label{eq:time_acceptance_power_law}
\end{equation}
where $a$ models the steepness of the turn-on while the exponent $n$ and the offset $b$ model the position of the turn-on.
The final parameter, $\beta$ is used to represent the relative reduction in events at larger decay-times.

The acceptance function is calculated using a combination of simulated and real data using the formalism given in Equation~\ref{eq:time_acceptance_power_law}.
Acceptance parameters are calculated on \BsDspi data and then calibrated based on a binned ratio between simulated \BsDspi and \BsDsK data.
This method of calibration relies on the assumption that \BsDspi is flavour-specific and that therefore the lifetime of the \Bs is well known.

The time acceptance is first fitted on simulated \decay{\Bs}{\Ds\pion} events with a reconstructed decay-time larger than $\unit{0.2}{\ps}$ and is shown in Figure~\ref{fig:time_acceptance_fit}.
The obtained parameters are given in Table~\ref{tab:time_acceptance_model} with correlations given in Table~\ref{tab:acceptance_model_correlations}.
A second fit is then performed on simulated \BsDsK events, the results of which are given in Table~\ref{tab:time-acceptance-fn-params_dsk}.

\begin{table}
 \centering
 \begin{tabular}{cS}
  \toprule
  \textbf{Parameter}  & \textbf{Value}   \\
  \midrule
  $a$                 & 1.42\pm0.204 \invps     \\
  $b$                 & 0.0230\pm0.0364  \\
  $n$                 & 1.81\pm0.066     \\
  $\beta$             & 0.0363\pm0.0118 \invps \\
  \bottomrule
 \end{tabular}
 \caption{Fitted parameters for the fit of the time acceptance model to simulated \BsDspi data.} \label{tab:time_acceptance_model}
\end{table}

\begin{table}
 \centering
 \begin{tabular}{c|SSSS}
  \toprule
  \textbf{Parameter}  & \textbf{$a$}  & \textbf{$b$}  & \textbf{$n$}  & \textbf{$\beta$}  \\
  \midrule
  $a$                 &  1            & 0.992         & 0.914         & -0.985            \\
  $b$                 &  0.992        & 1             & 0.874         & -0.973            \\
  $n$                 &  0.914        & 0.874         & 1             & -0.900            \\
  $\beta$             &  -0.985       & -0.973        & -0.900        & 1                 \\
  \bottomrule
 \end{tabular}
 \caption{Correlations of parameters for the fit of the time acceptance model to simulated \BsDspi data.} \label{tab:acceptance_model_correlations}
\end{table}

\begin{figure}[htbp]
  \centering
  \includegraphics[width=0.95\textwidth]{\currfiledir/time-acceptance-DsPi-cpowerlaw.pdf}
  \caption[The acceptance function fitted to simulated \BsDspi data.]{The acceptance function fitted to simulated \BsDspi data.
           The acceptance function from Equation~\ref{eq:time_acceptance_power_law} is overlaid in green, scaled up for clarity.}
  \label{fig:time_acceptance_fit}
\end{figure}

\begin{table}
  \centering
    \begin{tabular}{c|SSSS|r}
      \toprule
      \textbf{Parameters} & $\beta$ & \emph{n} & \emph{b} & \emph{a} & \textbf{Parameter value}\\
      \midrule
      $\beta$ & 1 & 0.657 & 0.501 & -0.784 & $(3.94 \pm 0.28) \times 10^{-2}$ \invps \\
      \emph{n} & 0.657 & 1 & 0.905 & -0.574 & $2.00 \pm 0.078$ \\
      \emph{b} & 0.501 & 0.905 & 1 & -0.276 & $(-1.73 \pm 1.23) \times 10^{-2}$ \\
      \emph{a} & -0.784 & -0.574 & -0.276 & 1 & $1.46 \pm 0.025$ \invps \\
      \bottomrule
    \end{tabular}
  \caption{A summary of the parameter values and correlations for the acceptance function for \BsDsK.} \label{tab:time-acceptance-fn-params_dsk}
\end{table}

Based on these best-fit values and covariance matrices, 1000 sets of acceptance parameters are created to represent the range of uncertainties in the fit to simulated data.
In \unit{0.1}{\ps} bins from \unit{0.2}{\ps} to \unit{10}{\ps} the mean and variance of the acceptance value is calculated for each of the decay modes.
The ratio is then taken between \BsDsK and \BsDspi in each bin and is shown in Figure~\ref{fig:acc_DsKDspi_ratio} to give a binned correction factor.

\begin{figure}
 \centering
 \includegraphics[width=0.95\textwidth]{\currfiledir/acceptance-ratio-cpowerlaw-mean-rms.pdf}
 \caption{Ratio of acceptance parameters between \BsDsK and \BsDspi as a function of decay-time.}
 \label{fig:acc_DsKDspi_ratio}
\end{figure}

Finally, a fit to real \BsDspi data is performed using the sFit method. In this fit the assumption is made that $\Cpar=1$ and that $\Spar = \Sbpar = \Dpar = \Dbpar = 0$.
The physics parameters \Gs, \DGs and \dms are also fixed to their nominal values as given in Table~\ref{tab:variables-fixed-in-time-fit}.
The only variables floated in the fit are the four parameters of the acceptance model,
the fitted values of which are shown in Table~\ref{tab:sfit_bsdspi_acc}.
The resultant PDF from this data fit is them scaled by the binned correction factor to give the acceptance function used in the final \BsDsK fit.

\begin{table}
 \centering
 \begin{tabular}{cS}
  \toprule
  \textbf{Parameter}             & \textbf{Fitted value} \\
  \midrule
  $\beta$        & 0.0363  \pm  0.0068  \invps          \\
  $n$            & 1.849 \pm  0.071        \\
  $b$            & 0.0373 \pm  0.0119          \\
  $a$            & 1.215 \pm  0.053     \invps \\
  \bottomrule
 \end{tabular}
 \caption{Acceptance parameters as a result of the fit to the real data \BsDspi time distribution.}
 \label{tab:sfit_bsdspi_acc}
\end{table}

\section{Fit to data}

The distribution given by plotting the decay-time distribution of \BsDsK candidates, weighted by their signal sWeights, is fitted using the maximum-likelihood method.
The \CP parameters $C$, $S_f$, $S_{\bar{f}}$, $D_f$ and $D_{\bar{f}}$ are all freely floated between $-3$ and $3$.
The values of the fitted parameters are given in Table~\ref{tab:sfit_bsdsk} along with their correlations in Table~\ref{tab:sfit_bsdskcorr}.

\begin{table}
 \centering
 \begin{tabular}{lS}
  \toprule
  \textbf{Parameter}    & \textbf{Fitted value} \\
  \midrule
  \Cpar                 &  1.01 \pm 0.50 \\
  \Spar                 & -1.25 \pm 0.56 \\
  \Sbpar                & 0.083 \pm 0.68 \\
  \Dpar                 & -1.33 \pm 0.60 \\
  \Dbpar                & -0.81 \pm 0.56 \\
  \bottomrule
 \end{tabular}
 \caption{CP observables fitted to the \BsDsK decay-time distribution.}
 \label{tab:sfit_bsdsk}
\end{table}

\begin{table}
 \centering
 \begin{tabular}{l|SSSSS}
  \toprule
  \textbf{Parameter}  & \Cpar    & \Dpar      & \Dbpar & \Spar     & \Sbpar \\
  \midrule
  \Cpar       & 1      & -0.155   & -0.137    & -.110   & 0.174     \\
  \Dpar       & -0.155 & 1        & 0.566     & -0.057  & -0.026    \\
  \Dbpar      & -0.137 & 0.566    & 1         & -0.025  & -0.016    \\
  \Spar       & -0.110 & -0.057   & -0.025    & 1       & -0.020    \\
  \Sbpar      & 0.174  & -0.026   & -0.016    & -0.020  & 1         \\
  \bottomrule
 \end{tabular}
 \caption{Correlation matrix of the \BsDsK \CP parameter fit.}
 \label{tab:sfit_bsdskcorr}
\end{table}

\section{Systematic uncertainties}

There are many sources of systematic uncertainty in the fit to the time-distribution of \BsDsK data.
How the various parameters are varied to estimate the magnitude of the uncertainties is explained later in Section~\ref{sec:systematic-sources}.
The calculated values for \BsDsK are summarised in Table~\ref{tab:totalsfitsyst_dsk}.

\begin{table}
 \centering
 \begin{tabular}{lSSSSS}
  \toprule
                             & \multicolumn{5}{c}{\textbf{Parameter}} \\
                             \cmidrule{2-6}
                             & \Cpar  & \Spar    & \Sbpar & \Dpar & \Dbpar \\
  \midrule
  \textbf{Statistical uncertainty}    & 0.50 & 0.56 &   0.68    & 0.60 & 0.56 \\
  \midrule
  \textbf{Systematic} ($\sigma_{\textrm{stat}}$) & \multicolumn{5}{c}{} \\
  Decay-time bias       & 0.03 &    0.05    & 0.05 &0.00 & 0.00 \\
  Decay-time resolution & 0.11 &    0.08    & 0.09 &0.00 & 0.00 \\
  Tagging calibration   & 0.23 &    0.17    & 0.16 &0.00 & 0.00 \\
  Background yields     & 0.15 &    0.07    & 0.07 &0.07 & 0.07 \\
  Physics parameters    & 0.15 &    0.22    & 0.20 &0.40 & 0.42 \\
  Asymmetries           & 0.12 &    0.01    & 0.04 &0.00 & 0.02 \\
  Momentum/Length Scale & 0.00 &    0.00    & 0.00 &0.00 & 0.00 \\
  k-Factors             & 0.27 &    0.27    & 0.27 &0.08 & 0.08 \\
  Bias correction       & 0.03 &    0.03    & 0.03 &0.03 & 0.03 \\
  \midrule
  %Total Systematic      & 0.23 &    0.24    & 0.28 &0.26 & 0.26 \\
  \textbf{Total systematic} ($\sigma_{\textrm{stat}}$)
  & 0.46 & 0.50 &   0.35    & 0.43 & 0.46 \\
  \bottomrule
 \end{tabular}
\caption{Total error budget for the decay-time fit. Systematic uncertainties are given as fractions of the statistical uncertainty. 
         Systematic uncertainties are added in quadrature under the assumption that they are uncorrelated.}
\label{tab:totalsfitsyst_dsk}
\end{table}

\section{Conclusion}

The final results for the \BsDsK decay are
\begin{alignat}{2}
 \Cpar   &= &&1.01  \pm 0.50 \pm 0.23, \\
 \Spar   &= -&&1.25 \pm 0.56 \pm 0.24, \\
 \Sbpar  &= &&0.08  \pm 0.68 \pm 0.28, \\
 \Dpar   &= -&&1.33 \pm 0.60 \pm 0.26, \\
 \Dbpar  &= -&&0.81 \pm 0.56 \pm 0.26, 
\end{alignat}
where the first uncertainties are statistical and the second uncertainties are systematic.

At this stage, no attempt is made to determine confidence intervals for physics parameters \rdsk, $\Delta$ and \gammabeta given by the relations in Equations~\ref{eq:dsk_phys_params_C}--\ref{eq:dsk_phys_params_Sbpar}.
This is because a full treatment would require correct understanding of the statistical and systematic covariance matrices since it has been found from simulation studies that they have a non-negligible effect on \gammabeta.
Given future work on this matter, this analysis will provide a unique measurement of the value of $\gamma$, unavailable to other particle physics experiments.
