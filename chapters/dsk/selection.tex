\chapter{Data selection} \label{chapter:dsk_selection}

\section{Data sample}

This analysis uses data from the 2011 run of \lhcb.
This comprises an integrated luminosity $\intlum{1.0 \,\invfb}$ of $\proton\proton$ collisions recorded at a centre-of-mass energy of $\sqrt{s}=\unit{7}{\tev}$.

\section{Simulated data}

Several samples of simulated data were created for the analysis, primarily for the use in event selection and background studies.
In each sample, a \B hadron is forced to decay to a specific final state as listed in Table~\ref{tab:dsk_mc_samples} along with the number of events generated for each channel.

\begin{table}[htbp]
 \centering
 \begin{tabular}{llr}
  \toprule
  \textbf{Sample} &         & \textbf{Sample size} \\
  \midrule
  \BsDspi         & \DsKKpi & 1052495 \\
  \BsDsK          & \DsKKpi & 1887293 \\
  \BsDsstpi       & \DsKKpi & 524098  \\
  \BsDsstK        & \DsKKpi & 206000  \\
  \BsDsrho        & \DsKKpi & 2019391 \\
  \BsDsstrho      & \DsKKpi & 1019191 \\
  \LbDsp          & \DsKKpi & 539994  \\
  \LbDsstp        & \DsKKpi & 630598  \\
  \LbLcpi         & \LcpKpi & 2033496 \\
  \LbLcK          & \LcpKpi & 519495  \\
  \BdDrho         & \DKpipi & 2054494 \\
  \BdDstpi        & $\Dstar \to D\piz$, \DKpipi
                            & 1046498 \\
  \BdDpi          & \DKpipi & 1016198 \\
  \BdDK           & \DKpipi & 958393 \\
  \BdDsK          & \DsKKpi & 517198 \\
  \bottomrule
 \end{tabular}
 \caption{Simulated samples used during the analysis for selection and background studies.}
 \label{tab:dsk_mc_samples}
\end{table}

\section{Reconstruction}

The \BsDsK decay mode is reconstructed in two stages,
first the \Ds candidate is created from its daughter particles and then a \kaon is added to make the \Bs meson.
The \Ds candidates are reconstructed in three separate final states, \DsKKpi, \DsKpipi and \Dspipipi each of which are selected as independent samples based on particle identification requirements.
The invariant mass of the combination of the three \Ds meson daughters is fixed to the nominal value of the \Ds meson when reconstructing the mass of the \Bs meson
and, conversely, when calculating the decay time of the \Bs meson, the mass of the \Ds meson is not constrained
but the momentum vector of the \Bs is required to point from the $\proton\proton$ primary vertex.

The sample is further divided based on the polarity of the magnet (\emph{up} or \emph{down})
as well as the flavour tagging information of the \Bs meson candidate (\Bs, \Bsb or untagged).
Thus there are 18 sub-samples of data with no events being present in more than one data set.

%In addition to this signal decay mode, a background decay mode of \BdDpi with \DsKpipi is also reconstructed and split into the same categories. This data sample is used to quantify the number of events belonging to this background mode which sit under the signal peak in the mass distribution.

\section{Event selection}

The event selection is performed in a four-step process, defined partially by the \lhcb experimental considerations.
The steps of the selection process are:
\begin{enumerate}
 \item trigger,
 \item experiment-wide offline selection (\emph{stripping}),
 \item analysis-specific offline selection,
 \item particle identification.
\end{enumerate}
The details of which are covered in the rest of this chapter.

\section{Trigger}

The first level of selection is performed by the \lhcb trigger system described in Section~\ref{sec:trigger_description}.
All events for this analysis are required to be those which contained the particles which the trigger used to make its decision.
This means that the track which activated the trigger is required to be used in the reconstruction of the signal candidate.
For an event to be considered, two independent trigger algorithms must have fired.
First, in the \hltone, a region of interest is defined by a straight line track in the \velo and then a single detached high-momentum track is required to be within that region.
This trigger (internally known as \texttt{1TrackAllL0}) is detailed in a dedicated note at Ref~\cite{LHCb-PUB-2011-003}.
Secondly, the \hlttwo trigger is required to have fired on the detection of a single, high-momentum track, displaced from the $\proton\proton$ collision point
and to have found a single similarly displaced vertex containing the detected track and 1--3 other tracks.
This trigger algorithm (called the 2-, 3- or 4-body \texttt{TopoBBDT}) is described by a public note at Ref~\cite{LHCb-PUB-2011-016}.

%No events which are \emph{triggered independent of signal} (TIS) are used as they only constitute a small part of the overall data set
%and including them would mean that the time acceptance would need to be treated differently.

\section{Stripping selection}

Stripping is performed centrally within the \lhcb collaboration and the results of it are made available to all through the standard \lhcb book-keeping processes.
Its primary purpose is to provide a number of data sets, each defined by a set of relatively loose selection criteria, to be used by numerous analyses within the \lhcb collaboration.
While the stripping selection is performed offline, after the events have been stored to disk, it is treated as a separate step to the \emph{offline selection}.

The selection (called a \emph{stripping line} within \lhcb) used to select the initial set of \Bs candidates for this analysis is the \texttt{Stripping\-B02DPiD2HHH\-Beauty2Charm\-Line} and it is performed as a two-step process.
First a loose pre-selection is made based on the kinematics of the particles and their displacement from the primary interaction.
All charged particles which are used to make the \Bs meson are required to have
a track $\chisq\textrm{/ndof} < 4$, $\pt > \unit{100}{\mevc}$ and $\ptot > \unit{1}{\gevc}$.
Finally, each track used to reconstruct the \Bs meson is, in turn, artificially combined with the tracks used to create the primary vertex.
If the $\chisq$ of this vertex combination is small ($\leqslant4$) then the given track is not used in the \Bs reconstruction.

To speed up the processing, additional requirements are placed on the \Ds meson candidate before its decay vertex is created:
the scalar sum of the \pt of the particles used to create it must be $> \unit{1.8}{\gevc}$,
the largest \emph{distance of closest approach} (DOCA) of the particles with respect to the primary vertex must be larger than $\unit{0.5}{\mm}$
and the reconstructed invariant mass must be within \unit{100}{\mevcc} of the nominal \Dp or \Ds meson mass.
After the vertex has been formed, final requirements of a vertex $\chisq\textrm{/ndof} < 10$ 
and that the vertex is well separated from the primary collision vertex are imposed.

After the initial pre-selection, remaining events are passed through a \emph{bagged boosted decision tree} (BBDT) \cite{BBDT}.
It is trained using the \pt of the \Bs meson candidate, the separation of its decay vertex from the primary vertex and a combination of the \chisq/ndof of the \Bs meson and \Ds meson vertices.
The BBDT response value is required to be $> 0.05$ to give the distributions shown in Figures~\ref{fig:dsk_data_after_stripping} and \ref{fig:dsk_mc_after_stripping}.

\begin{figure}
  \centering
    \includegraphics[width=0.49\textwidth]{\currfiledir/strippingDsKData_mass.pdf}
    \includegraphics[width=0.49\textwidth]{\currfiledir/strippingDsKData_time.pdf}
  \caption{Distributions for \BsDsK candidates in data after the stripping selection.}
  \label{fig:dsk_data_after_stripping}
\end{figure}

\begin{figure}
  \centering
    \includegraphics[width=0.49\textwidth]{\currfiledir/strippingDsKMC_mass.pdf}
    \includegraphics[width=0.49\textwidth]{\currfiledir/strippingDsKMC_time.pdf}
  \caption{Distributions for simulated \BsDsK decays after the stripping selection.}
  \label{fig:dsk_mc_after_stripping}
\end{figure}

\section{Offline selection} \label{sec:offline_selection}

The offline selection is run over the output of the stripping selection and is composed of a number of parts.
First a boosted decision tree selection is used which is trained on kinematic and topological information. 
Then PID requirements are applied for the \Ds daughters and bachelor pion and finally a set of vetoes for \D, \Lc and \jpsi decays are set.

\subsection{Boosted decision tree training}

At the core of the event selection process is a gradient boosted decision tree (BDTG) which is trained on \BsDspi, \DsKKpi data.
A BDTG is a binary tree classifier which involves making multiple yes or no decisions on an event, each based on a single variable until a certain stop condition is met.
Two data sets are passed through the system to train it, one representative of the signal and one representative of any expected backgrounds.
Depending on whether the majority of events ending up in a given leaf node are signal or background, all the events in that end up in that node are labelled as such.
The boosting is performed by maintaining multiple trees at a time and combining them at the end to produce a continuous number.
Thus, using the trained tree, each event in a data set can be assigned a value between 0 and 1 to be used as a signal discriminant.
The BDTG implementation used is that from \tmva \cite{tmva}.

The data set is split into two equal-sized parts, one to be used for training the BDTG and the other to test its response.
Each sample contains an equal amount of magnet-up and magnet-down data.

A sample of events to represent the combinatorial background in the BDTG training is taken from the upper sideband of the reconstructed \Bs meson, defined as $m(\Bs) > \unit{5445}{\mevcc}$.
This data set is taken from the output of the stripping as defined above.

The signal training sample is extracted from \BsDspi, \DsKKpi data after the stripping selection.
To improve the performance of the selection, it is preferable to train on signal from data.
In order to subtract the background events from the sample, the \emph{sPlot} technique \cite{sPlot} is used.
First, the events from the stripping selection have an additional pre-selection applied to them as given in Table~\ref{tab:BDTGselection}
and then they are processed using the \emph{sPlot} technique.
The sPlot technique works by assigning a weight (called an \emph{sWeight}) to each event in the sample describing how signal-like it is.
Based on a maximum-likelihood fit, the sWeights are given by
\begin{equation}
  W_{n}(y_e)=\frac{\sum_{j=1}^{N_s} V_{nj}f_j(y_e)}{\sum_{k=1}^{N_s} N_{k} f_{k}(y_e)},
 \label{eq:sweights}
\end{equation}
where $V_{nj}$ is the covariance matrix resulting from the likelihood maximisation,
$N_k$ is the number of events expected on the average for the $k^{\rm th}$ component,
$f_i(y_e)$ is the value of the PDFs of the discriminating
variables $y$ for the $i^{th}$ component and for event $e$,
$N_s$ denotes the number of components.
In this case, the discriminating variable is the \Bs mass and there are two components: a Gaussian function for the signal and an exponential for the background.

\begin{table}
 \centering
 \begin{tabular}{lc}
  \toprule
  \textbf{Description}  & \textbf{Requirement} \\
  \midrule
  Bachelor        & \dllkpi $< 0$ \\
  Both kaons      & \dllkpi $> 0$ \\
  \Ds mass        & $[1940, 1990]\mevcc$ \\
  \Dp veto:       & \\
  $\quad$ \dllkpi of same charge $K$  &  $>10$, or \\
  $\quad$ \Ds under \Dp hypothesis &  below $1850\mevcc$ \\
  \Lc veto:    & \\
  $\quad$ $p$ veto, same charge $K$ &  \dllkpi-\dllppi $>5$, or \\
  $\quad$ \Ds under \Lc hypothesis & not in $[2250, 2320]\mevcc$ \\
  \bottomrule
 \end{tabular}
 \caption{Additional pre-selection requirements applied to \BsDspi, \DsKKpi used in BDTG optimisation.}
 \label{tab:BDTGselection}
\end{table}

The fit is performed on the full mass range of data after the selections given in Table~\ref{tab:BDTGselection}.
It is done in two steps, first with all shape and yield parameters floating to extract the shape parameters.
Then, the fit is performed again with only the yields floating to avoid the correlations between the shapes and yields entering the covariance matrix.
The outcome of this is an sWeight for each event in the sample which can be used in the BDTG training to subtract the background.

From all the possible kinematic and topological variables that could be used to train the BDTG, only a subset is used.
They are chosen by utilising \tmva to calculate their importance as a discrimination variable in the tree.
Those which are used in the final selection are given in Table~\ref{tab:bdtvariables}
and the BDTG response distributions for the training and test samples are given in Figure~\ref{fig:BDTGResponse}.

\begin{table}[ht]
\centering
\begin{tabulary}{\linewidth}{>{\everypar{\hangindent2.2em}}L}
\toprule
\textbf{Variable}\\
\midrule
$B_s$:                                                       \\
$\quad$  Cosine of the angle between the momentum vector and the line from the primary vertex and the \Bs decay vertex    \\
$\quad$  The $\chi^2$ distances of the track the primary vertices                                                         \\
$\quad$  Radial flight distance                              \\
$\quad$  Vertex $\chi^2$ divided by ndof                     \\
$\quad$  Lifetime vertex $\chi^2$ divided by ndof            \\
\midrule
Bachelor:\\
$\quad$ The minimum of the $\chi^2$ distances of the track from any of the primary vertices                               \\
$\quad$ $p_T$                                                \\
$\quad$ $\cos(\theta)$                                       \\
\midrule
$D_s$:   \\
$\quad$ Cosine of the angle between the momentum vector and the line from the \Ds origin vertex and the \Ds decay vertex  \\
$\quad$ Cosine of the angle between the momentum vector and the line from the primary vertex and the \Ds decay vertex     \\
$\quad$ The minimum of the $\chi^2$ distances of the track from any of the primary vertices                               \\
$\quad$ Radial flight distance                               \\
$\quad$ Vertex $\chi^2$ divided by ndof                      \\
\midrule
$D_s$ children: \\
$\quad$  Minimum $p_T$                                       \\
$\quad$  The minimum of the $\chi^2$ distances of the track from any of the primary vertices                              \\
\midrule
Bachelor and $D_s$ children  \\
$\quad$  Maximum track ghost probability                     \\
\bottomrule
\end{tabulary}
\caption{Input variables to the BDTG.}
\label{tab:bdtvariables}
\end{table}

\begin{figure}
  \centering
  \includegraphics[width=.49\textwidth]{\currfiledir/BDTGResponse_tr.pdf}
  \includegraphics[width=.49\textwidth]{\currfiledir/BDTGResponse_ch.pdf}
  \caption{BDTG response distributions for training (left) and test (right) samples.
    The red histogram corresponds to signal weighted by the signal sWeights,
    while the blue shows background weighted by the combinatorial background sWeights. }
  \label{fig:BDTGResponse}
\end{figure}

\subsection{Selection optimisation}

The BDTG response requirement is chosen to give maximum signal significance which is defined as
\begin{equation}
 S = \frac{N_{sig}}{\sqrt{N_{sig}+N_B}}
\end{equation}
where $N_{sig}$ and $N_B$ are the signal and sum of all backgrounds respectively.

The yields for this significance are extracted from the data using the nominal mass fit described in Section~\ref{sec:mass-fit}.
A full scan across the values of the BDTG response is performed with the requirement being scanned from 0.0 to 0.8 in 0.05 increments.
The signal significance for these scans is shown in Figure~\ref{fig:selOptPid} and Figure~\ref{fig:selOptMassfits} shows the mass fit at two selected points in the scan.

\begin{figure}[htb]
  \centering
  \includegraphics[width=.75\textwidth]{\currfiledir/selOptAllPID10.pdf}
  \caption{Significance scan for the BDTG response cut value.}
  \label{fig:selOptPid}
\end{figure}

\begin{figure}[htb]
  \centering
  \includegraphics[width=\textwidth]{\currfiledir/selOptPID10BDT25.pdf} \\
  \includegraphics[width=\textwidth]{\currfiledir/selOptPID10BDT50.pdf}
  \caption[Mass fits to \BsDsK data candidates for two selection working points.]{Mass fits to \BsDsK data candidates for two selection working points. Top: BDTG response $> 0.25$ Bottom: BDTG response $> 0.50$. These fits are fully described later in Chapter~\ref{chapter:dsk_mass_fit}.}
  \label{fig:selOptMassfits}
\end{figure}

The signal significance is observed to range between $25\sigma$ and $30\sigma$ and a final requirement of the BDTG response being larger than 0.5 is imposed.

\section{Particle identification} \label{sec:pid}

Events which pass the BDTG selection are then further refined using PID requirements.
These are defined using the logarithm of the likelihood of having a certain detector response given the hypothesis that the particle is a given particle, minus the same given the hypothesis of it being a pion
This gives a set of \emph{delta-log-likelihoods} (DLLs) which should discriminate between different particle types.
Of most interest here is the \dllkpi which can be used to select between kaons and pions, though the \dllppi is also used to reject protons.

It would be preferable to perform a significance scan across a range of \dllkpi (perhaps a range of $-10$ to $20$)
but since changing the particle identification requirement would require recomputing the shapes of the backgrounds used in the mass fit, only two values of \dllkpi are tested.
Testing at $\dllkpi>5$ and $\dllkpi>10$ gives significances of the signal yield of $29.5\sigma$ and $30.5\sigma$ respectively.
More importantly, the tighter requirement greatly reduces the cross-feed contribution of \BsDspi under the signal peak.
Therefore, a particle identification requirement of $\dllkpi>10$ on the bachelor track is applied.

The requirements placed on the various final-state particles are given in Table~\ref{tab:pidselection}.

\begin{table}
\centering
\begin{tabular}{lll}
  \toprule
  \textbf{Applied to}  & \textbf{Description}  & \textbf{Requirement} \\
  \midrule
  \BsDspi              & Bachelor pion         & $\dllkpi > 10$   \\
  \DsKKpi              & Both kaons            & $\dllkpi > 0$   \\
  \DsKpipi             & Kaon                  & $\dllkpi > 10$  \\
                       & Both pions            & $\dllkpi < 5$   \\
  \Dspipipi            & All pions             & $\dllkpi < 10$  \\
                       & All pions             & $\dllppi < 10$  \\
  \bottomrule
\end{tabular}
\caption{PID selection requirements.}
\label{tab:pidselection}
\end{table}

\subsection{Background vetoes}

A number of specific vetoes are needed in order to reduce the large number of \Dp mesons coming from \BdDpi and similar decays.
These also reject contributions from long-lived \Lb decays such as \LbLcpi where the proton from \decay{\Lc}{\proton\Km\pip}  is misidentified as a kaon.
\decay{\jpsi}{\mumu} decays are vetoed in the case where both muons are misidentified as pions.
Finally, \Dz decays such as \decay{\Dz}{\Kp\Km} are also vetoed. All the vetoes used are given in Table~\ref{tab:vetoes}.

\begin{table}[bhtp]
 \centering
 \begin{tabular}{llc}
  \toprule
  \textbf{Applied to}  & \textbf{Description}  & \textbf{Requirement} \\
  \midrule
  \DsKKpi              & \Dz veto:    & \\
                       & $\quad$ $m(\KpKm)$   & $<1840\mevcc$ \\
                       & \Dp veto:    & \\
                       & $\quad$ \dllkpi of same charge $K$ & $>10$, or \\
                       & $\quad$ $|m(\KpKm) - 1020\mevcc|$ & $<10\mevcc$, or \\
                       & $\quad$ \Ds under \Dp hypothesis & not in $[1840, 1900]\mevcc$ \\
                       & \Lc veto:    & \\
                       & $\quad$ $p$ veto, same charge $K$ & \dllkpi$-$\dllppi $>5$, or \\
                       & $\quad$ \Ds under \Lc hypothesis & not in $[2250, 2320]\mevcc$ \\
  \midrule
  \DsKpipi             & \Dz veto:    & \\
                       & $\quad$ $m(\Kp\pim)$ & $<1750\mevcc$ \\
                       & \Lc veto:    & \\
                       & $\quad$ $p$ veto on kaon & \dllkpi$-$\dllppi $>0$, or \\
                       & $\quad$ \Ds under \Lc hypothesis & not in $[2250, 2320]\mevcc$ \\
                       & \jpsi veto: & \\
                       & $\quad$ Both $m(\pip\pim)$ & $|m-m(\jpsi)|>30\mevcc$ \\
  \midrule
  \Dspipipi            & \Dz veto:    & \\
                       & $\quad$ Both $m(\pip\pim)$ & $<1700\mevcc$ \\
  \bottomrule
 \end{tabular}
 \caption{Vetoes applied on \Ds meson candidates.}
 \label{tab:vetoes}
\end{table}

The distribution of the key variables for both real and simulated data after all the selection requirements 
are shown in Figure~\ref{fig:dsk_after_offline_selection}.

\begin{figure}
  \centering
    \includegraphics[width=0.44\textwidth]{\currfiledir/Offline_Bs2DK_merged_mass.pdf}
    \includegraphics[width=0.44\textwidth]{\currfiledir/Offline_Bs2DK_merged_ctau.pdf}
    \includegraphics[width=0.44\textwidth]{\currfiledir/Offline_MC_Bs2DsK_Ds2KKPi_mass.pdf}
    \includegraphics[width=0.44\textwidth]{\currfiledir/Offline_MC_Bs2DsK_Ds2KKPi_ctau.pdf}
  \caption{Distributions for real (top) and simulated (bottom) \BsDsK data after the offline selection.}
  \label{fig:dsk_after_offline_selection}
\end{figure}
