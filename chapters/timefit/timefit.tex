\chapter{Decay-time fit} \label{chapter:time_fit}

In order to extract the \CP parameters,
a fit to the reconstructed proper decay-time distribution of the \B meson candidates is performed.
The yields from the mass fit are used to fix the yields in the decay-time fit.

In contrast to the \BsDsK analysis, the fit to the time distribution of \BsDspi candidates is performed using a conventional fit rather than the sFit method.
This is because it was found to be difficult to control the correlations between the parameters in the sFit, leading to large statistical uncertainties.

\section{Background classification}

The decay time behaviour of the various backgrounds to the \BsDspi decay fall into two main categories:
\begin{itemize}
 \item Some do not oscillate at all and so can be modelled as a simple exponential decay.
 \item Others are neutral \Bz or \Bs mesons which oscillate while propagating through the detector. Of these types, there are two sub-categories:
 \begin{itemize}
  \item Those which only decay to a single, flavour-specific, final state.
        For example, \Bd can only decay into a single final state, $f$, while \Bdb can only decay into a final state $\bar{f}$.
  \item Those which oscillate in flight but also decay with a contribution from a wrong-flavour final state.
        This means that \Bd can also decay into $\bar{f}$ and \Bdb can decay into $f$. These are referred to as \emph{\decay{\Bd}{\D\pion}-like}.
 \end{itemize}
\end{itemize}
The backgrounds that were discussed in Chapter~\ref{chapter:backgrounds} considered in this fit are grouped according to Table~\ref{tab:time-fit-background-groups}.

\begin{table}
 \centering
 \begin{tabular}{ccc}
  \toprule
  \textbf{Oscillating with wrong-flavour} & \textbf{Flavour specific} & \textbf{Non-oscillating} \\
  \midrule
  \BdDpi                   & \BdDspi            & \LbLcpi \\
  \BdDrho                  & \BdDsstpi          & Combinatorial         \\
  \BdDstpi                 & \BsDsrho           &                       \\
                           & \BsDsstpi          &                       \\
                           & \BsDsstrho         &                       \\
  \bottomrule
 \end{tabular}
 \caption{Categories for the physics backgrounds in the time fit.} \label{tab:time-fit-background-groups}
\end{table}

For the backgrounds that are flavour-specific, the \CP parameters from Equation~\ref{eq:decay_rates} are fixed to be $C_f = C_{\bar{f}} = 1$ and $\Sf = \Sfbar = \Df = \Dfbar = 0$.
Meanwhile, the background modes with possible wrong-flavour contributions are fitted with those values fixed to their measured physical values.
For the signal mode they are freely floated without constraint.
In the standard parameterisation of Equation~\ref{eq:decay_rates}, the four \Sf, \Sfbar, \Df and \Dfbar parameters are strongly correlated with each other.
In order to remove these correlations in the signal mode,
the \CP variables are instead parameterised as
\begin{equation}
 \begin{split}
  \overline{S} &= \frac{\Sf+\Sfbar}{2} \\
  \overline{D} &= \frac{\Df+\Dfbar}{2}
 \end{split}
 \quad
 \begin{split}
  \Delta S &= \frac{\Sf-\Sfbar}{2} \\
  \Delta D &= \frac{\Df-\Dfbar}{2} .
 \end{split}
 \label{eq:cp-parameters-avg-delta}
\end{equation}

\section{PDFs and fit setup}

The signal mode is treated specially and independently of the other modes and is modelled using the time-dependent equations in Section~\ref{sec:time-evolution-b-meson}.
The \CP parameters are treated as in Equation~\ref{eq:cp-parameters-avg-delta} and are floated freely.
From Equation~\ref{eq:cp_function_of_lambda} it can be seen that while \Sf, \Sfbar, \Df and \Dfbar are mostly linearly dependent on \modlambdaf,
$C$ depends quadratically on it.
Since the expected value of \modlambdaf is small (in the case of a flavour-specific decay it will be 0), $C-1$ is expected to be very small and so $C$ is fixed to be $1$.
In addition to this, \Gs and \DGs (as defined in Equation~\ref{eq:m_deltam_ganna_deltagamma}) are also fixed based on an \lhcb measurement of \BsToJPsiPhi \cite{LHCb-CONF-2012-002} to the values given in Table~\ref{tab:variables-fixed-in-time-fit}.
They are fixed in order to reduce the number of free parameters in the fit to speed it up and also to help stability given the strong correlation between \DGs and \deltad.

The only other physical parameter floated is \dms.
While allowing this parameter to freely float will negatively affect the determination of some of the \CP parameters due to strong correlations,
it was decided that fixing \dms to a published value would potentially bias any measurement of the \CP parameters of the signal mode.
This is because the best measurements from \lhcb were measured on our signal mode
and many historical measurements were made under the assumption which this analysis is testing.
Therefore, \dms is allowed to float freely between \unit{5.0}{\invps} and \unit{30.0}{\invps} with an initial value of \unit{17.719}{\invps}.

\begin{table}
 \centering
 \begin{tabular}{cS}
  \toprule
  \textbf{Parameter} & \textbf{Value}      \\
  \midrule
  \Gd             & \SI{0.656}{\per\ps}   \\
  \DGd            & \SI{0}{\per\ps}       \\
  \Gs             & \SI{0.658}{\per\ps}   \\
  \DGs            & \SI{-0.116}{\per\ps}  \\
  \dmd            & \SI{0.507}{\per\ps}   \\
  $\Gamma_{\Lb}$  & \SI{0.719}{\per\ps}   \\
  $\Gamma_{comb}$ & \SI{0.800}{\per\ps}   \\
  \bottomrule
 \end{tabular}
 \caption{Parameters which are fixed in the fit and their values.} \label{tab:variables-fixed-in-time-fit}
\end{table}

The three non-flavour-specific backgrounds (\decay{\Bd}{\D\pion}, \decay{\Bd}{\D\rho} and \decay{\Bd}{\Dstar\pion})
are again modelled using the time-dependent equations in Section~\ref{sec:time-evolution-b-meson}.
The \CP parameters for these three backgrounds are known and published by previous experiments at \babar and \belle \cite{dpi_cp_babar_partially, dpi_cp_babar_fully, dpi_cp_belle_partially, dpi_cp_belle_fully}.
The world-averages of these combined measurements are published by the Heavy Flavour Averaging Group (HFAG) and are given in Table~\ref{tab:CP_background_generation_values}.
These \CP parameters are fixed in the fit as are \Gd, \DGd and \dmd as given in Table~\ref{tab:variables-fixed-in-time-fit}.

\begin{table}
 \centering
 \begin{tabular}{cScS}
  \toprule
  \textbf{Mode}             & \textbf{Strong phase}  & \textbf{Weak phase}   & $|\lambda_f|$  \\
  \midrule
  \decay{\Bd}{\D\pion}      & 0.0                    & $-\nicefrac{\pi}{2}$  & 0.012          \\
  \decay{\Bd}{\Dstar\pion}  & 0.0                    & $-\nicefrac{\pi}{2}$  & 0.015          \\
  \decay{\Bd}{\D\rho}       & 0.0                    & $-\nicefrac{\pi}{2}$  & 0.038          \\
  \bottomrule
 \end{tabular}
 \caption[Input \CP parameters for non-flavour-specific backgrounds.]{Input \CP parameters for non-flavour-specific backgrounds. Calculated from HFAG \cite{HFAG2012}.} \label{tab:CP_background_generation_values}
\end{table}

The flavour-specific oscillating modes are grouped into two categories, those coming from a \Bz and those coming from a \Bs.
The same base PDF model is used as in the signal mode but since it is known
that the flavour-specific modes have \CP parameters of $\Sf = \Sfbar = \Df = \Dfbar = 0$,
they are fixed to these values in the fit.
As with the non-flavour-specific backgrounds, the \Bz modes have \Gd, \DGd and \dmd fixed
while the \Bs modes have only \Gs and \DGs fixed with \dms being floated and shared with the signal mode.

The two non-oscillating modes are each modelled with a single exponential decay function, the lifetimes for which are fixed.
The values of $\Gamma$ used are based on the mean \Lb lifetime for the \Lb mode and \unit{0.8}{\invps} for the combinatorial background.

As for modelling the tagging behaviour, the modes are grouped and are each given an independent tagging efficiency.
The signal mode has its own tagging efficiency
while the three non-flavour-specific \Bd modes are grouped together as are the two flavour-specific \Bd modes.
The three \Bs backgrounds are also grouped.
The combinatorial and the \Lb backgrounds each have their own individual tagging efficiency.
The tagging efficiency for each group is floated freely in the fit between $0.0$ and $1.0$.
The fit is performed with per-event mistag probabilities rather than using a single average mistag across all events.

\subsection{Decay-time acceptance}

Previously, in the \BsDsK analysis, the acceptance function was calculated using a multi-step process as described in Section~\ref{sec:decay-time-acceptance}.
First a fit to simulated \BsDspi data was performed which was then scaled by a ratio between simulated \BsDspi and \BsDsK.
Finally, this model was fitted to real \BsDspi data in order to match the properties of the real data distribution.

Since that process relied on the fact that \BsDspi was flavour-specific, it is not possible to use that result in this analysis
and so the result after just the first step of the fit to the simulated \BsDspi data is used as given in Table~\ref{tab:time_acceptance_model}.

\subsection{Blind fit to data}

In order to test that the time fit converges correctly,
a fit to data is performed.
To avoid the results of this fit biasing the analysis,
the true central values of the fit are kept hidden.
The statistical uncertainties are kept intact however and are shown in Table~\ref{tab:blind_fit}.

\begin{table}[hbtp]
 \centering
 \begin{tabular}{cS}
  \toprule
  \textbf{Parameter}  & \textbf{Statistical uncertainty}  \\
  \midrule
  \avgs               & \pm0.150                      \\
  \avgd               & \pm0.098                     \\
  \deltas             & \pm0.083                     \\
  \deltad             & \pm0.050                     \\
  \bottomrule
 \end{tabular}
 \caption{Statistical uncertainties from the blind fit to data.} \label{tab:blind_fit}
\end{table}
