\chapter{The \lhcb detector} \label{chapter:detector}

\section{The \lhc}

The \lhc is a proton-proton collider located at \cern, near Geneva in Switzerland.
It is a circular accelerator, \unit{27}{\km} in circumference, consisting of straight acceleration sections and bending sections.

Protons are produced from a hydrogen duoplasmatron source and are accelerated to \unit{50}{\mev} by a linear accelerator.
They are fed into the Proton Synchrotron Booster where they are further accelerated up to \unit{1.4}{\gev}.
From here they are passed into the Proton Synchrotron (PS) where they are separated into bunches and accelerated to \unit{25}{\gev}.
After the PS, the protons (in bunches) are accelerated by the Super Proton Synchrotron to an energy of \unit{450}{\gev}
before being injected into the main \lhc rings.
Under design conditions, the \lhc would contain 2808 bunches, each containing $1.15\times10^{11}$ particles.
The bunches move in opposite directions around the ring and are collided at 4 distinct points.
The \lhc was designed to run at a centre of mass energy of \unit{14}{\tev} at a peak luminosity of \unit{10^{34}}{\invcma\invsec}.

There are 4 main experiments located at the \lhc collision points: \alice, \atlas, \cms and \lhcb.
The luminosity at the \lhcb collision point is reduced to \unit{3-4\times10^{32}}{\invcma\invsec} by defocusing and offsetting the beams
to reduce the number of interactions per bunch crossing down to an average of $1.6$.

Proton-proton collisions ran at the \lhc from April 2011 until October 2011.
During the 2011 run of data taking, on which the analyses reported within this thesis are performed, 
\unit{1.2}{\invfb} of integrated luminosity was delivered by the \lhc of which \unit{1.1}{\invfb} was recorded by \lhcb as shown in Figure~\ref{fig:2011-int-lum}.
However, only about \unit{1.0}{\invfb} are actually available for physics analyses since some data had to be discarded due to poor quality.
The protons in the 2011 run were collided with a centre-of-mass energy of $\sqrt{s} = \unit{7}{\tev}$.

\lhcb recorded an average number of interactions per bunch crossing of between 1 and 2 (Figure~\ref{fig:2011-peak-mu}) which is above the design specification of 0.4.
It also ran with a higher than design level instantaneous luminosity of around \unit{3\mbox{--}4\times10^{32}}{\invcma\invsec} (Figure~\ref{fig:2011-peak-inst-lum}).

\begin{figure}
  \centering
    \includegraphics[width=0.95\textwidth]{\currfiledir/2011-int-lum.png}
  \caption{Integrated luminosity delivered to LHCb (blue) and recorded by LHCb (red) during 2011 proton-proton collisions.}
  \label{fig:2011-int-lum}
\end{figure}

\begin{figure}
  \centering
    \includegraphics[width=0.95\textwidth]{\currfiledir/2011-peak-mu.png}
  \caption{Peak interactions per bunch crossing ($\mu$) per \lhc fill at LHCb in 2011.}
  \label{fig:2011-peak-mu}
\end{figure}

\begin{figure}
  \centering
    \includegraphics[width=0.95\textwidth]{\currfiledir/2011-peak-inst-lum.png}
  \caption{Peak instantaneous luminosity per \lhc fill at LHCb in 2011.}
  \label{fig:2011-peak-inst-lum}
\end{figure}

\section{\lhcb}

\lhcb \cite{lhcb_jinst} is designed to focus on the investigation of \CP violation in \B meson decays and searches for physics beyond the standard model.
There are many analyses that are being performed to investigate \CP violation and numerous stand-alone
results can hope to be found. Some of the main results being looked for by \lhcb are more
accurate measurement of the angles of the unitarity triangles, branching fractions of heavy meson
decays and searches for new physics.

\begin{figure}
  \centering
    \includegraphics[width=0.95\textwidth]{\currfiledir/lhcb.png}
  \caption[A side view of the \lhcb detector showing the primary interaction point of the collider at the far left.]{A side view of the \lhcb detector showing the primary interaction point of the collider at the far left. Particles produced will travel towards the right and be detected by the various components. Reproduced from Ref~\cite{lhcb_jinst}.}
  \label{fig:lhcb-detector}
\end{figure} 

The \lhcb detector (Figure~\ref{fig:lhcb-detector})
is a single arm spectrometer with angular coverage from \unit{10}{\mrad}
to \unit{250}{\mrad} in the vertical plane (\unit{300}{\mrad} in the horizontal).
It is placed at one of the crossing points of the \lhc (intersection point 8) where bunches of protons going in opposite directions will collide.
\lhcb is designed for the detection of \B (containing an \bquarkbar quark) and \Bbar (containing a \bquark quark) mesons from this collision.
At the high energies at which the \lhc operates the \B and \Bbar mesons will be preferentially produced
in the longitudinal direction in a fairly tight cone (see Figure~\ref{fig:b-hadron-pythia}).
That is, the direction of most of the mesons will be along the beam pipe in either direction
--- creating two cones with their points touching at the primary collision vertex.
On average the two cones will be identical.
For this reason and due to cost constraints the detector is placed to cover just one of the cones.
This is different to many other particle detectors (such as \atlas and \cms)
which completely encase the interaction point so as to try to capture all particles produced.

\begin{figure}
 \centering
   \includegraphics[width=0.7\textwidth]{\currfiledir/tp_theta.eps}
 \caption[The simulated production angle of \B hadrons with respect to the beam line.]{The simulated production angle of \B hadrons with respect to the beam line. Reproduced from Ref~\cite{LHCbTR}.}
 \label{fig:b-hadron-pythia}
\end{figure} 

The focus on \B physics study puts many requirements on the design of the detector such as
a need for excellent primary and secondary vertex resolution to allow for precise measurements of the proper decay time
as well as good momentum resolution. Further to this, excellent particle identification and efficient triggering is needed. 
These are essential for the study of neutral \B meson oscillations and decays.

The \lhcb detector is designed to run at a much lower luminosity (a factor 50 lower) than the \lhc's nominal value
to reduce the average number of proton-proton collisions per bunch crossing to a much lower number.
By design the number of collisions per bunch crossing, $\mu$, was $0.4$ while actual running conditions were closer to $1.4$.
A process known as \emph{luminosity levelling} is used to both reduce the total instantaneous luminosity as well as provide a constant luminosity throughout a run.
The two beams are offset from each other in the vertical direction to reduce the cross-section of the overlap of the two beams.
As the run continues and the bunches are steadily depleted of protons,
the beams are gradually brought back together to maintain the average number of collisions.
If the beams were simply defocused
then at the beginning of the run
there would be too many collisions per crossing
and by the end of the run there would be too few.

The detector comprises several sub-detectors, each of which serves a different purpose and can be
categorised into two groups --- tracking detectors and particle identification (PID) detectors.

\subsection{The Vertex Locator (\velo)}
\label{sec:velo}

The first sub-detector that a particle produced in the initial collision traverses is the Vertex
Locator which is a tracking detector.
This serves to measure the position of vertices --- that is where the products of the proton-proton collision decay
as well as the primary proton-proton collision vertex.
Within it are a series of about 20 pairs (called stations) of sensor plates or ``modules'' (see Figure~\ref{fig:velo})
each of which record the positions of particles passing through in 2 dimensions.
If a particle passes through at least two of the stations
then it is (in principle) possible to trace back the path to the point at which it was created
and the \velo is designed such that any track
which is within the angular acceptance of the rest of the \lhcb detector
will pass through at least 3 stations.
As can be seen from the figure, the position of the modules is such that only particles with very small or very large
($\unit{15}{\mrad} > \phi > \unit{300}{\mrad}$) angle will escape without being tracked. The total coverage of the \lhcb
detector is defined by $\phi < \unit{250}{\mrad}$.
The more stations that a particle travels though, the more accurate the measurement of the vertex will be.
The resolution of the \velo is designed to be about \unit{4}{\mum} for particles at $\phi = \unit{100}{\mrad}$.

\begin{figure}
 \centering
   \includegraphics[width=0.95\textwidth]{\currfiledir/VELO.png}
 \caption[Layout of the \velo subdetector. This detector closely surrounds the primary interaction region of the collider.
          Shown is a slice through the VELO at $y=0$ (top-down).]{Layout of the \velo subdetector. This detector closely surrounds the primary interaction region of the collider.
          Shown is a slice through the VELO at $y=0$ (top-down). The red and blue segments are the sensor modules.
          Reproduced from Ref~\cite{lhcb_jinst}.}
 \label{fig:velo}
\end{figure}

Separating the \velo stations from the vacuum of the beam is the RF foil which is a pair of shaped aluminium sheets each containing half the modules.
The RF foil serves two purposes, firstly it is there to protect the LHC vacuum from outgassing of the \velo modules.
It also acts as a shield to protect the \velo from radio-frequency pickup from the LHC beams
as well as protecting the beam from wake fields which are generated as the beams pass through the \velo.

Each module contains both an R and \mphi sensor and sits on one side of the beam line.
The general layout is shown in Figure~\ref{fig:velo-module-layout}.
Both modules are silicon strip detectors arranged in a semi-circular annulus
with an outer radius of \unit{41.9}{\mm} and an inner radius of \unit{8}{\mm}.
The inner radius is designed to be as small as possible to get close to the interaction point
but is restricted by a minimum safe distance to the beam (\unit{5}{\mm}),
\unit{1}{\mm} for the guard structures on the silicon and to leave space for the RF foil.

\begin{figure}
 \centering
   \includegraphics[width=0.6\textwidth]{\currfiledir/velo-module-layout.pdf}
 \caption[Layout of a pair of \velo modules. Shown are both radial and \mphi strips.]{Layout of a pair of \velo modules. Shown are both radial and \mphi strips. Reproduced from Ref~\cite{lhcb_jinst}.}
 \label{fig:velo-module-layout}
\end{figure}

The R sensors consist of 512 strips running concentrically.
Within each module the sensors are divided into 4 segments.
This segmentation reduces primarily strip occupancy but also reduces the strip capacitance.
The pitch of each strip increases radially
to keep the occupancy per strip approximately constant
as the strip length increases and the particle flux decreases.
The pitch varies from \unit{38}{\mum} at the inner edge of the sensor %Check number
out to \unit{101.6}{\mum} at the outer edge.

The \mphi sensors are divided into two sections radially
again to keep the occupancy low
and to stop the strip pitch from getting too large at high radii.
The strips are not aligned perfectly in the radial direction
but are skewed at an angle of \unit{20}{\degrees} in the inner section
and \unit{-10}{\degrees} in the outer section with respect to the radial direction.
In alternating stations the angles are reversed to create a stereo effect.
The pitch of the strips increases linearly towards the outer edge of the sensor.
It is \unit{35.5}{\mum} at the inner edge of the inner section and increases to a value of \unit{78.3}{\mum} at the section border.
The outer section then starts from a pitch of \unit{39.3}{\mum} and increases up to \unit{97}{\mum}.

The \velo provides excellent vertex resolution which is required for the types of analyses \lhcb performs.
As can be seen in Figure~\ref{fig:velo-resolution},
the primary vertex resolutions when there are 25 tracks are: 
$\sigma_x = \unit{13.1}{\mum}$, $\sigma_y = \unit{12.5}{\mum}$, $\sigma_z = \unit{71.1}{\mum}$
which are very close to the design values.

\begin{figure}
 \centering
   \includegraphics[angle=-90,width=0.49\textwidth]{\currfiledir/ResXY_1PV_2012Data.pdf}
  \includegraphics[angle=-90,width=0.49\textwidth]{\currfiledir/ResZ_1PV_2012Data.pdf}
 \caption[Resolution of the \velo with respect to different numbers of tracks (N) for events with one primary vertex from data collected in 2011.]{Resolution of the \velo with respect to different numbers of tracks (N) for events with one primary vertex from data collected in 2011. On the left is the $x$ (red line) and $y$ (blue line) resolution and on the right is the $z$ resolution. Reproduced from Ref~\cite{velo-performance}.}
 \label{fig:velo-resolution}
\end{figure}

\subsection{Ring Imaging Cherenkov (\rich) detector}

Within \lhcb there are two \rich detectors, both used for particle identification --- most importantly
to differentiate between pions and kaons in the decay of \B mesons. The first (\richone, shown in Figure~\ref{fig:rich}) covers
low momentum ($\sim\unit{1-60}{\gevc}$) particles while the one further downstream (\richtwo) covers higher
momentum particles (\unit{15}{\gevc} and higher).

\begin{figure}
%   \centering
  \begin{subfigure}[b]{0.49\textwidth}
    \centering
    \includegraphics[width=\textwidth]{\currfiledir/rich1-2d_detailed.eps}
    \caption[\richone: Particles approach from the left after leaving the VELO and pass out the right-hand side.]{\richone: Particles approach from the left after leaving the VELO and pass out the right-hand side. Reproduced from Ref~\cite{lhcb_jinst}.}
    \label{fig:rich}
  \end{subfigure}
  ~
  \begin{subfigure}[b]{0.49\textwidth}
    \centering
    \includegraphics[width=\textwidth]{\currfiledir/CKAnglevsMom_NoTheory_jun2011.eps}
    \caption[Reconstructed Cherenkov angle, $\theta$, against the momentum of various tracks when passing through the \ce{C4 F10} radiator.]{Reconstructed Cherenkov angle, $\theta$, against the momentum of various tracks when passing through the \ce{C4 F10} radiator. Reproduced from Ref~\cite{LHCb-DP-2012-003}}
    \label{fig:rich-pid}
  \end{subfigure}
  \caption{Ring Imaging Cherenkov (RICH) detectors.}
\end{figure} 

In both of the detectors, spherical and flat mirrors are used to focus Cherenkov light produced by the
particles travelling through the medium on to a set of hybrid photo-detectors (HPDs). The \richone
detector uses a combination of aerogel and \ce{C4 F10} while \richtwo uses just \ce{CF4}. The difference in
the radiator material and the layout of the mirrors provide good PID over a range of momenta.
This is shown in Figure~\ref{fig:rich-pid} where it can be seen that the combination of the aerogel and gas in
\richone provides excellent particle differentiation --- particularly for kaons and pions --- across the
range of momenta which is interesting for \lhcb analyses as well as for leptons at lower energies.

Both \rich detectors use Hybrid Photon Detectors (HPDs) to detect the Cherenkov light.
An example of one is shown in Figure~\ref{fig:hpdphoto}.
There are 196 HPDs in \richone and 288 HPDs in \richtwo placed in two planes in each detector.
They are arranged in a hexagonal pattern to provide the best coverage given their circular profile.

An incoming photon will interact with the photocathode layer on in the inside of the quartz window and produce a photoelectron.
The electron is accelerated by a \unit{20}{\kev} voltage onto a silicon pixel array at the back of the HPD.
The spatial resolution of a HPD is \unit{2.5}{\mm} with a time resolution of \unit{25}{\ns}.

\begin{figure}
 \centering
    \includegraphics[width=0.6\textwidth]{\currfiledir/hpdphoto.jpg}
  \caption[Photograph of one of the HPDs used inside the \rich detector.]{Photograph of one of the HPDs used inside the \rich detector. Reproduced from Ref~\cite{lhcb_jinst}.}
  \label{fig:hpdphoto}
\end{figure}

For analyses, the main output of the \rich system is a set of PID variables called \emph{delta log-likelihoods} (DLLs).
For each event, initially all tracks in the event are assumed to be pions.
Based on this assumption, the probability distribution of finding photons in each HPD is calculated.
This probability distribution is compared against the observed hits to calculate a PID likelihood.
The hypothesis of each particle is then changed in turn to be a kaon, a proton and so on,
and for each of these alternative hypotheses a global likelihood is once again calculated.
The values of the likelihoods of the pion hypothesis and, for example, the kaon hypothesis is used to calculate the PID variable \dllkpi which is defined as
\begin{equation}
\dllkpi = \ln{\mathcal{L}(K)} - \ln{\mathcal{L}(\pi)},
\end{equation}
where $\mathcal{L}(K)$ is the global likelihood given the kaon hypothesis (and likewise for \pion). This means that \dllkpi can be used as a differentiator between kaons and pions.
The efficiency of kaon identification as a function of particle momentum is shown in Figure~\ref{fig:kaon-pid-eff}.

\begin{figure}
 \centering
    \includegraphics[width=0.8\textwidth]{\currfiledir/KPi_Separation.eps}
  \caption[Kaon identification efficiency (in red) and pion misidentification as kaon rate (in black) requirements for data as a function of track momentum.]{Kaon identification efficiency (in red) and pion misidentification as kaon rate (in black) requirements for data as a function of track momentum. Two different $\Delta\log\mathcal{L}(K-\pion)$ have been imposed on the samples, resulting in the open and filled markers. Reproduced from Ref~\cite{LHCb-DP-2012-003}.}
  \label{fig:kaon-pid-eff}
\end{figure}

\subsection{Tracking}

The tracking subsystem comprises four subdetectors: the \velo (as discussed in Section~\ref{sec:velo}), the Tracker Turicensis (\ttracker), the Inner
Tracker (\intr) and the outer tracker (\ot).

\subsubsection{Silicon Tracker}

\begin{figure}
  \begin{subfigure}[b]{\textwidth}
    \centering
      \includegraphics[width=0.75\textwidth]{\currfiledir/tt_layer.eps}
    \caption{Layout of the third \ttracker detection layer. The different shadings indicate different readout sections.}
    \label{fig:tt-layer}
  \end{subfigure}
  
  \begin{subfigure}[b]{\textwidth}
    \centering
      \includegraphics[width=0.75\textwidth]{\currfiledir/it_layer.eps}
    \caption{Layout of an $x$ detection layer in the second \intr layer.}
    \label{fig:it-layer}
  \end{subfigure}
  \caption[Layout of the Silicon trackers.]{Layout of the Silicon trackers. Reproduced from Ref~\cite{lhcb_jinst}.}
\end{figure} 

Since the \ttracker and the \intr use the same technology, they are together referred to as the Silicon Tracker.
The \ttracker is just upstream of the main magnet and the \intr is positioned downstream of the magnet.
As their names suggests, they are tracking detectors, used to accurately locate the positions of
particles in order to be able to reconstruct their paths.

Each detector is constructed as strips of silicon with an average pitch of \unit{200}{\mum}.
The \ttracker is made up of four layers
with the second and third layer placed at an angle ($-5\degrees$ and $+5\degrees$ respectively)
and each layer is placed approximately \unit{30}{\cm} apart.
A schematic view of the third layer can be seen in Figure~\ref{fig:tt-layer} where the $+5\degrees$ angle is visible.

\begin{figure}
  \centering
    \includegraphics[width=0.75\textwidth]{\currfiledir/OT.pdf}
  \caption[Positions of the \ttracker and \intr with respect to the Outer Tracker.]{Positions of the \ttracker and \intr with respect to the Outer Tracker.
           The \ttracker is on the left of the figure, upstream of the Inner and Outer Trackers.
           The \intr (shown here in purple) is the smaller tracker near the beam pipe on the right, surrounded by the \ot.}
  \label{fig:silicon-tracker-positions}
\end{figure}

The \intr covers a small area of the \lhcb acceptance near to the beam pipe. It is made up of three layers and is shown in Figure~\ref{fig:silicon-tracker-positions}.
The second and third layers are placed at a stereo angle as in the \ttracker (the layout of the second layer is shown in Figure~\ref{fig:it-layer}).

Both trackers have a resolution of approximately \unit{50}{\mum}.

\subsubsection{Outer Tracker}

The outer tracker is placed at the same $z$ positions as the \intr and covers the rest of the angular acceptance (see Figure~\ref{fig:silicon-tracker-positions}).
It uses arrays of drift tubes filed with 70\% \ce{Ar} and 30\% \ce{CO2}.
It provides a spatial resolution of \unit{200}{\mum}
and, as with the silicon trackers, the second and third layers are placed at an angle of $-5\degrees$ and $+5\degrees$ with respect to the vertical axis.

\subsection{Magnet}

\begin{figure}
  \centering
    \includegraphics[width=0.75\textwidth]{\currfiledir/magnet.eps}
  \caption[The \lhcb dipole magnet.]{The \lhcb dipole magnet. Reproduced from Ref~\cite{lhcb_jinst}.}
  \label{fig:magnet}
\end{figure}

\begin{figure}
  \centering
    \includegraphics[width=0.6\textwidth]{\currfiledir/beamaxis-2.eps}
  \caption[The magnetic field strength measured on-axis as a function of $z$.]{The magnetic field strength measured on-axis as a function of $z$. Reproduced from Ref~\cite{lhcb_jinst}.}
  \label{fig:magnet-strength}
\end{figure}

The \lhcb dipole magnet (shown in Figure~\ref{fig:magnet}) is placed downstream of the \ttracker but before the first inner and outer tracking station.
It provides a vertical magnetic field to enable measurement of the momenta and charges of particles.
The opening in the centre of the magnet is designed to be large enough to sit entirely outside the acceptance of the rest of the detector.
In order to reduce systematic uncertainties, particularly in \CP measurements, the magnet's polarity can be inverted.
During normal running, the magnet was set to each configuration for approximately equal amounts of time.

In order to achieve the necessary momentum resolution, the magnet provides a peak field strength of about \unit{1.1}{T}.
The strength of the field is measured throughout the interior of the magnet with a Hall probe.
The strength of the field is shown in Figure~\ref{fig:magnet-strength}.

\subsection{Calorimeter}

The calorimeter system in \lhcb contains four sections:
the scintillator pad detector (\spd),
preshower detector (\presh),
an electromagnetic calorimeter (\ecal)
and a hadron calorimeter (\hcal).
It measures the energy and position of particles by providing a heavy target to cause showers of particles.
It provides particle identification for photons, electrons and hadrons.
Since neutral particles will leave no trace in the tracking system,
reconstructing neutral pions and prompt photons in the calorimeter is essential for studies of many decays.

The \spd/\presh detector uses two layers of scintillator pads sandwiching a \unit{15}{\mm} \ce{Pb} converter plate.
Its main purpose is to validate and cross-check any signals that the \ecal receives.
Any neutral particles will not interact with the scintillator pads which allows the \ecal to differentiate between high energy photons and electrons.
It is also used for global event cuts to measure event activity.

\begin{figure}
  \centering
    \includegraphics[angle=-90,width=0.49\textwidth]{\currfiledir/fig_2_1.pdf}
    \includegraphics[angle=-90,width=0.49\textwidth]{\currfiledir/fig_2_2.pdf}
    \caption[Lateral segmentation of the SPD/PS and ECAL and the HCAL.]{Lateral segmentation of the SPD/PS and ECAL (left) and the HCAL (right). One quarter of the detector front face is shown.
    In the left figure the cell dimensions are given for the ECAL. Reproduced from Ref~\cite{lhcb_jinst}.}
    \label{fig:calo-geom}
\end{figure}

The \ecal is built with layers of scintillating tiles (\unit{4}{\mm} thick) alternating with lead (\unit{2}{\mm} thick) acting as active material and absorber respectively.
Readout is achieved with wavelength-shifting fibres, embedded in the scintillator tiles which read out into phototubes.
The \ecal is split into three sections as shown in Figure~\ref{fig:calo-geom}
due to the fact that the track hit density varies by two orders of magnitude over the surface of the detector.

The \hcal's main purpose is to provide information for the hadron trigger
and so is designed to have a very fast response time,
even at the expense of good energy resolution.
The design of the \hcal is similar to that of the \ecal
so it uses alternating layers of scintillator and steel absorber plates.
As seen in Figure~\ref{fig:calo-geom},
the \hcal is only split into two sections
due to the wider shape of hadronic showers.


\subsection{Muon system}

The muon system is the last stage in the \lhcb detector --- providing identification of muons.
The precise measurement and identification of muons is essential to many of the \lhcb measurements
as they are present in the final state of many interesting \B meson decays.

Being able to accurately distinguish muons from other particles is important for many of the key measurements that \lhcb is making,
such as the search for new physics in measurements of the branching fraction of \decay{\Bs}{\mumu}.
Being able to cleanly separate muons from other particles is critical to be able to measure decay channels with a very low branching fraction (a few $10^{-9}$) for the decay \decay{\Bs}{\mumu}.

The muon system provides information for the \lzero high-\pt muon trigger
as well as muon identification for both the \hlt and for offline analysis.

\begin{figure}
  \centering
    \includegraphics[width=0.5\textwidth]{\currfiledir/muon-positions.eps}
  \caption[The position of the muon detectors with respect to the calorimeters.]{The position of the muon detectors with respect to the calorimeters. Reproduced from Ref~\cite{lhcb_jinst}.}
  \label{fig:muon-detector-positions}
\end{figure}

The muon system consists of five rectangular stations as shown in Figure~\ref{fig:muon-detector-positions}.
The first muon station (\Mone) is upstream of the calorimeters and the other 4 (\Mtwo-\Mfive) are downstream.
\Mone is used to improve the \pt measurement for the trigger.
The key differentiator to identify a muon is that most particles will stop within the calorimeter system
whereas only muons will manage to travel all the way through to \Mtwo-\Mfive. Consequently any particles found in those systems are very likely to be muons.
This provides a very clean muon signal.

There are two types of detector technology used in the muon system.
The centre region of \Mone (nearest the beam pipe) uses a triple Gas Electron Multiplier (\gem)
and the rest of \Mone and the entirety of \Mtwo-\Mfive use Multi-Wire Proportional Chambers.
The detectors provide point measurements of particle tracks, giving a binary decision for whether a track was detected.
Stations \Mone-\Mthree provide good spatial resolution in the $x$ direction and are used to calculate the \pt of the muon (with a resolution of 20\%) along with the track direction.
The final two stations have much more limited spatial resolution being mainly used for particle identification.

\subsection{Trigger} \label{sec:trigger_description}

\begin{figure}
  \centering
    \includegraphics[width=0.75\textwidth]{\currfiledir/trigger-schematic-dave.pdf}
  \caption[Overview of the \lhcb trigger system.]{Overview of the \lhcb trigger system. Reproduced from Ref~\cite{LHCb-TALK-2012-089}.}
  \label{fig:trigger-overview}
\end{figure}

Almost all \lhc bunch crossings will produce interesting data for physics analyses.
However, due to storage space constraints, only a very small number of these can be fully recorded.
To ensure that the most interesting events are stored, an efficient trigger is required.

The \lhcb trigger works on a two-level system as shown in Figure~\ref{fig:trigger-overview}
where the first trigger level (\lzero) is a hardware trigger and the high-level trigger (\hlt) is a software trigger.
The \lzero uses only a limited amount of information from the detector.
It is used to automatically determine whether any particular proton-proton collision event is interesting
and so whether it should be subjected to further analysis
--- making its decision only \unit{4}{\mus} after the initial collision.
The input to the \lzero is at a rate of up to \unit{40}{\mhz} while it has to output at only \unit{1}{\mhz}
which is a limit set by the maximum rate of the readout electronics.
There are two main parts of the \lzero trigger, the calorimeter trigger and the muon trigger.
The calorimeter trigger uses information from the \ecal and \hcal to select tracks with high transverse energy (\et)
and to select the photons, electrons, \piz and hadron candidates with the highest \et.
It uses information from the \presh and \spd to aid with the particle identification.
The muon trigger selects muon candidates with high \pt.
To fire the trigger, a track must be present in all five of the muon stations, pointing to the interaction point.

The second level of the the trigger, called the ``High-Level Trigger'' (\hlt), 
is split into two stages (\hltone and \hlttwo) and is implemented in software.
While the \lzero trigger only uses partial information from certain subdetectors,
the \hlt performs partial reconstruction of the event to be able to have access to higher level information such as reconstructed tracks.
\hltone consists of a set of approximately 20 trigger selection algorithms
each of which makes a decision based on the presence of one or two tracks (such as two muons or a single hadron)
which match certain criteria, for example having high \pt.
It outputs at a maximum rate of \unit{40}{\khz}.
The \hlttwo stage then runs over those events which passed the \hltone.
The \hlttwo attempts to reconstruct the event in a way as similar to the full offline reconstruction as possible
and triggers on inclusive decay signatures or the presence of exclusive charm and beauty hadron candidates.

\section{\lhcb software}

The \lhcb software framework is built on \gaudi \cite{GAUDI} which provides an extensive framework for building HEP data analysis tools.
It is an object-oriented \cpp toolkit, providing all the common tasks that analysis software would need such as data access and histogram management.
Tools can be build using the framework in a modular fashion allowing the fast switching of components as the task requires.

\subsection{Simulated event production}

In order to perform in depth studies of the detector and for physics analyses, Monte-Carlo (MC) simulated events are used.
Within \lhcb, event generation is performed by two applications, \gauss \cite{gauss} and \boole \cite{boole} and is broken into a number of steps:
\begin{description}
\item[Event generation]
\gauss uses \pythia \cite{pythia} to simulate the interaction and scattering of proton-proton interactions.
This produces elementary particles and hadrons which are propagated and decayed using \evtgen \cite{evtgen}.
\item[Event simulation]
Once the particles have been generated, their interaction with the \lhcb detector is simulated using \geant \cite{geant}.
Their interaction with the bulk of the detector is simulated as well as with sensitive parts of the detector
such as energy deposits in the silicon detectors and Cherenkov photon creation in the \rich detectors.
\item[Digitisation]
The simulated interaction with the detector results in energy being present at a specific place in the silicon and a number of photons being present in the RICH's HPDs.
\boole The \cite{boole} platform is used to simulate the detector's response to these signals and convert it into virtual readout channels as the physical detector would.
After this step, the data format and contents should be identical to that of real data which allows the same algorithms to be run over both simulated and real data.
\end{description}

The \hlt software is managed by a piece of software called \moore \cite{moore}.
This is usually run automatically as part of the trigger setup
but it can also optionally be run ``offline'' on simulated events to simulate the trigger
and so end up with an identical selection of events to real data.

\subsection{Reconstruction}

Reconstruction of events in \lhcb is performed using \brunel \cite{brunel}.
It is able to run identically over simulated or real data since by this stage they should be identical in format.
Reconstruction of the particle tracks based on the detector response is performed using pattern recognition algorithms.
These tracks are then used in particle identification (PID) process to assign particle types to each track.

\subsection{Analysis}

The final stage in the \lhcb software chain is \davinci \cite{davinci}.
It is a \gaudi-based framework providing all the information needed to perform an analysis
such as kinematic information about the particles, PID information and overall event information.
Starting from a list of the particles in an event it can find decay chains
and create a \root \cite{root} ntuple containing the relevant events.

%\section{Distributed computing and GANGA}

%\todo{This section is still to do.}

%\lcg \cite{lcg}

%\ganga \cite{ganga}

%\section{Data collected in 2011}
