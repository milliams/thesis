\chapter{Preface}

The \lhcb detector at the \lhc is dedicated to two primary aims.
The study of \CP violation in the quark sector and searches for new physics, beyond the Standard Model.
The two aims are not mutually exclusive as precision measurements of \CP parameters could shed light on new physics processes.
To date the Standard Model of particle physics has withstood repeated attempts to measure inconsistencies,
from the earliest days of experimental particle physics up to the latest results from the experiments at the \lhc.
Most recently, the precise measurement of \decay{\Bs}{\mumu},
which so far fantastically agrees with Standard Model predictions
despite hopes that it would yield evidence of physics beyond the Standard Model.

This thesis presents an analysis testing one of the assumptions underlying many other precision analyses at \lhcb and elsewhere -- that there are no wrong-flavour contributions to the decay of \BsDspi.
By searching for wrong-flavour contributions to the \BsDspi decay I hope to start work on constraining our level of certainty of this assumption.

The thesis is composed of three main parts.
The first provides a background to the analysis work carried out by explaining in Chapter~\ref{chapter:theory} the theoretical underpinning of the models used when analysing \lhcb data.
This is followed in Chapter~\ref{chapter:detector} by a detailed description of the \lhcb detector and the hardware and software analysis that is performed on the data collected.

Since the analysis of \BsDspi was based on a previous study of \BsDsK, Part~\ref{part:dsk_analysis} describes the analysis of that decay.
The analysis was written up in full \cite{LHCb-ANA-2012-068} and was submitted as a conference paper \cite{LHCb-CONF-2012-029}.
Chapter~\ref{chapter:dsk_introduction} introduces the analysis in the context of the \BsDspi analysis.
Chapters~\ref{chapter:dsk_selection} to \ref{chapter:dsk_time_fit} then describe the analysis work itself
including the selection of events, the fit to the \Bs candidate mass distribution and to the decay-time distribution.

Part~\ref{part:dspi_analysis} contains a description of the measurement of the \CP parameters of \BsDspi decays using \lhcb data in order to measure the wrong-flavour contribution to the decay.
Chapter~\ref{chapter:data_selection} describes the process of selecting the \BsDspi events from the \lhcb data.
Chapter~\ref{chapter:tagging} describes the algorithms and techniques used to tag the initial-state flavour of the \Bs mesons which is necessary for this type of time-dependent analysis.
This is followed in Chapter~\ref{chapter:backgrounds} by a description of the method by which the signal and background yields were extracted.
The fit to the decay-time distribution of \BsDspi candidates is presented in Chapter~\ref{chapter:time_fit}
with a full study of the possible sources of systematic uncertainty given in Chapter~\ref{chapter:systematics}.

Chapter~\ref{chapter:summary} gives a summary of the results of the \BsDspi analysis, comparing them to the results of the \BsDsK analysis and drawing conclusions.

Information on the combination of uncertainties given large correlations is given in Appendix~\ref{appendix:error_propagation}.
