\chapter{Flavour tagging} \label{chapter:tagging}

The flavour composition of the \Bs meson at the time of its creation is measured using a collection of algorithms.
Each algorithm is known as a \emph{tagger} and their results are combined into a final decision.
The taggers used in the \lhcb experiment are described fully in \cite{Grabalosa:1456804,LHCb-CONF-2011-003}.

In the proton-proton interaction, quarks are created in pairs.
In the case of a \bbbar pair being created, one of the quarks will hadronise to form a \B meson (perhaps via a $B^*$ or $B^{**}$) which will then decay as our measured signal.
The other will also form a \B hadron of some kind (labelled as the \emph{opposite-side}), the flavour content of which will be directly related to the flavour of the `signal \B meson'.
It is also possible that the partner to the signal \B meson's other valence quark (an \squark in our case) will also hadronise and form a kaon or a pion (known as \emph{associated production}) whose charge will be correlated with the signal \B meson's flavour.
The taggers fall in to two main categories: \emph{same-side (SS) taggers} which extract information on the \B meson flavour from kaons or pions emitted from the signal \B meson's intermediate $B^*$ or $B^{**}$ state and \emph{opposite-side (OS) taggers} which base their decision on the decay of the opposite-side \B hadron.
The layout of the taggers is shown in Figure~\ref{fig:tagging_schema}.
In the analysis shown in the thesis, only the opposite-side taggers are used.

\begin{figure}
  \centering
    \includegraphics[width=\textwidth]{\currfiledir/tagschema.pdf}
  \caption{Schematic of the various taggers used in \lhcb.}
  \label{fig:tagging_schema}
\end{figure}

There are four individual opposite-side taggers in total, each using a different facet of the decay to give an estimate of the initial flavour of the \Bs meson.
There are two which are based on the direct semi-leptonic decay of the opposite-side hadron.
In this case, the hadron decays via \decay{\bquark}{\cquark\Wm} with \decay{\Wm}{\mun\neumb} (or \decay{\Wm}{\en\neueb})
and the muon or electron's charge is linked to the flavour of the opposite-side \bquark hadron which in turn is related to the flavour of the signal \B meson.
Another tagger based on the decay of the \bquark hadron is the \emph{opposite-side kaon tagger}
in which a kaon created via a $\bquark\to\cquark\to\squark$ decay is detected and again the charge gives the flavour of the signal \B meson.
Finally, there is a tagger which collects together all the tracks which could be used to form an opposite-side decay chain and sums their electric charge.

Each tagger has certain sources of uncertainty associated with it. Some are irreducible and some are due to inefficiencies in the detector or selection of particles.
For example, there is a chance that if the opposite-side \bquark hadron is a \Bz or a \Bs then it might oscillate before decaying, causing the tagging algorithm to give the wrong answer.
There is also the chance of simply selecting the wrong muon or electron from the semi-leptonic decay and instead picking up a background particle which has no relation to the system of interest at all.
The ability for a tagger to get the wrong answer is called the \emph{mistag probability} (or \emph{mistag fraction} when talking about an ensemble of events), \mistag.
Each tagger will have a different average value of \mistag.

Of course, there are also situations when a particular tagger simply can't give an answer.
For example, the opposite-side \bquark hadron might not decay semi-leptonically in which case there is no electron or muon to detect.
Some taggers may even be mutually exclusive, for instance it is impossible to create both a pion and a kaon via associated production with the signal \B meson in a single event.
This effect is accounted for as a \emph{tagging efficiency}, \etag, which again will potentially be different for each individual tagger.

The \CP violating parameters being measured in the \BsDsK fit are proportional to the dilution, $D$, which is related to the mistag probability \mistag as
\begin{equation}
 D = 1-2\mistag
\end{equation}
and the statistical precision of \CP parameters is directly related to this by an effective efficiency, \effeff, given by
\begin{equation}
 \effeff = \etag D^2
\end{equation}
which is derived using the propagation of uncertainty~\cite{Grabalosa:1456804}.

\section{Calibration}

For each event, each tagger provides an initial estimate of the probability that it gave the wrong answer.
This probability, $\eta$, is calculated using a neural network based on event properties (such as total number of tracks) as well as the kinematic and geometrical properties of the particles used to provide the decision.
The neural network is trained on simulated data and so must be calibrated on real data to give a reliable result.
This calibration is performed on a channel where the signal \B meson will not oscillate (such as \decay{\Bp}{\jpsi\Kp}) so that the true flavour is known and can be compared to the estimated flavour.
All the events in the sample are collected in bins of $\eta$ and for each bin a mistag fraction, \mistag, is calculated based on how many events were tagged correctly.
The resulting values of \mistag are plotted against $\eta$ and its dependence is fitted with a linear function such as
\begin{equation}
 \omega(\eta) = p_0 + p_1 \times(\eta - \langle \eta \rangle),
 \label{eq:mistag_calibration}
\end{equation}
where $p_0$ and $p_1$ are fitted parameters and $\langle \eta \rangle$ is the average $\eta$ across the whole data set.
This function, along with the values of $p_0$ and $p_1$ can then be applied to any estimated $\eta$ when performing an analysis.

It is possible to estimate the systematic uncertainty on the calibration parameters by performing the calibration under a number of varying conditions.
By splitting the calibration data sample by magnet polarity and by the flavour of the signal meson and fitting each sample independently, a variation can be measured.
Additionally, altering the model used to fit the data distribution can have an effect on the calibration.
Adding these effects together in quadrature gives an estimate of the total systematic uncertainty.

\section{Combination}

Since each individual tagger is potentially giving an incorrect answer or even no answer for a given event, improved sensitivity can be gained by combining the answers from multiple, calibrated taggers.
The combined probability, $\mathcal{P}(\bquark)$ of the \B meson containing a \bquark or \bquarkbar quark is given by
\begin{equation}
 \mathcal{P}(\bquark) = \frac{p(\bquark)}{p(\bquark)+p(\bquarkbar)}, \qquad \mathcal{P}(\bquarkbar) = 1 - \mathcal{P}(\bquark).
\end{equation}
where $p(\bquark)$ is the probability to have a \bquark-tagged meson as a response from the combined tagger.
It is defined as
\begin{equation}
 p(\bquark) = \prod_i \left( \frac{1+d_i}{2} - d_i (1 - \mistag_i) \right),
\end{equation}
where $i$ labels each tagger in the combination and $d_i$ is the decision of the tagger such that $d_i=1$ means that the signal meson contains a \bquarkbar and $d_i=-1$ means that the signal meson contains a \bquark.

The final decision is made by comparing the two probabilities $\mathcal{P}(\bquarkbar)$ and $\mathcal{P}(\bquark)$.
If $\mathcal{P}(\bquark) > \mathcal{P}(\bquarkbar)$ then the signal meson is tagged as containing a \bquark with a mistag probability of $\eta^{comb} = \mathcal{P}(\bquarkbar)$ and conversely for the case where $\mathcal{P}(\bquarkbar) > \mathcal{P}(\bquark)$.

This estimated mistag probability, $\eta^{comb}$, does not take into account the correlations between the individual taggers
and so it is necessary to recalibrate the combined tagger once again against data using the same method as before to give a final value of \wcomb.
For the combination of opposite-side tagging algorithms used in this analysis, the calibration coefficients are measured on \decay{\Bp}{\jpsi\Kp} data
and are calculated to be $p_0 = 0.392 \pm 0.002 \pm 0.009$, $p_1 = 1.035 \pm 0.021 \pm 0.012$ and $\langle \eta \rangle = 0.391$
where the uncertainties are statistical followed by systematic.
The fit is shown in Figure~\ref{fig:comb_calib}.

\begin{figure}
  \centering
    \includegraphics[width=0.65\textwidth]{\currfiledir/comb_calib.pdf}
  \caption{Fit for the calibration of the combined flavour taggers.}
  \label{fig:comb_calib}
\end{figure}

% \begin{table}
%  \centering
%  \begin{tabular}{ccc}
%   $p_0$                        & $p_1$                        & $\langle \eta \rangle$  \\
%   \midrule
%   $0.392 \pm 0.002 \pm 0.009$  & $1.035 \pm 0.021 \pm 0.012$  & $0.391$                 \\
%  \end{tabular}
%  \caption[Calibration coefficients given for the opposite-side combination of taggers measured on \decay{\Bp}{\jpsi\Kp}.]{Calibration coefficients given for the opposite-side combination of taggers measured on \decay{\Bp}{\jpsi\Kp}. Uncertainties are statistical followed by systematic.}
%  \label{tab:tagging_calibration}
% \end{table}

\section{Per-event mistag probability}

There are two ways to use the tagging information.
First is to simply use an average mistag probability as a single parameter for every event in the fit.
However, in order to best exploit the tagging information provided by the tagging algorithms, a \emph{per-event} mistag probability is assigned.
This mistag probability, \mistag, is given by the calibrated estimate of the mistag probability provided by the tagging algorithm.

In tests performed on \decay{\Bp}{\jpsi\Kp} and \decay{\Bz}{\jpsi\Kstarz} improvements in \effeff of up to 60\% were measured \cite{Grabalosa:1456804}.
As such, for this analysis, per-event mistag probabilities are used.

\section{Possible optimisation of the tagger}

The standard taggers in \lhcb make use of simple, rectangular cuts in order to select the candidates used to determine the flavour tag.
It has been seen in many physics analyses that by replacing rectangular cuts with more complex selection methods such as boosted decision trees or neural networks,
it is possible to increase the event selection efficiency while keeping backgrounds low.

As part of my service work throughout my Ph.D., I performed a study into the possible improvements that could be made by using NeuroBayes \cite{NeuroBayes}, a neural network training package.
The opposite-side muon tagger and the same-side pion taggers were the subject of the study, with the aim of improving the \effeff compared with the standard \lhcb taggers.
The results of this work was published as an internal \lhcb note as Ref~\cite{LHCb-INT-2011-046}.

For each of the two taggers, a separate neural network was trained in order to tune the tagger to the specific requirements of the candidate selection.
Each neural network was trained on simulated data of the decay \decay{\Bp}{\jpsi\Kp}.
In order to test the dependence of the network on the training channel, a neural network for the opposite-side muon tagger was also trained using \decay{\B}{{\D}X} data where $X$ can be a pion or a kaon.

All three resulting networks were tested on \decay{\Bp}{\jpsi\Kp} data and the results compared with the charge of the \B meson.
The results for the muon tagger are shown in Table~\ref{tab:muon-nn-results}. The two neural network results differ only by the sample used to train the network.
The same-side pion performance is shown in Table~\ref{tab:pion-nn-results}. This neural network was both trained and tested on the \decay{\Bp}{\jpsi\Kp} channel.

\begin{table}[tb]
 \centering
 \begin{tabular}{rSSS}
 \toprule
 {\textbf{Tagger}} & {\etag (\%)} & {\mistag (\%)} & {\effeff (\%)} \\
 \midrule
 Standard & 5.15 & 29.3 \pm 1.9 & 0.88 \pm 0.11 \\
 \decay{\B}{{\D}X} Neural Network & 12.44 & 36.0 \pm 1.2 & 1.00 \pm 0.12 \\
 \decay{\Bp}{\jpsi\Kp} Neural Network & 9.91 & 34.0 \pm 1.2 & 0.99 \pm 0.10 \\
 \bottomrule
 \end{tabular}
 \caption{The results of the neural network-based tagger compared to the existing opposite-side muon tagger}
 \label{tab:muon-nn-results}
\end{table}

\begin{table}[tb]
 \centering
 \begin{tabular}{rSSS}
 \toprule
 {\textbf{Tagger}} & {\etag (\%)} & {\mistag (\%)} & {\effeff (\%)} \\
 \midrule
 Standard & 10.34 & 39.4 \pm 1.2 & 0.47 \pm 0.08 \\
 Neural Network & 89.87 & 46.0 \pm 1.0 & 0.58 \pm 0.09 \\
 \bottomrule
 \end{tabular}
 \caption{The results of the neural network-based tagger compared to the existing same-side pion tagger}
 \label{tab:pion-nn-results}
\end{table}
