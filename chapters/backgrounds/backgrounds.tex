\chapter{Data sample composition} \label{chapter:backgrounds}

There are a number of physics backgrounds which contaminate the data set.
It is important to be able to describe and quantify these contributions in order to account for them when extracting \CP parameters.
In order to extract the \CP parameters, a decay time fit is performed in which the yields of the signal and background channels are fixed
to values extracted from a fit to the \Bs mass distribution.

\section{Backgrounds to the mass fit}

In order to extract yields by fitting reconstructed mass distribution of the \Bs meson, it is important to understand the shapes of possible background contributions.

The backgrounds fall into two main categories:
partially reconstructed backgrounds, where one or more particles in the physical decay was not picked up by the reconstruction,
and fully reconstructed backgrounds, where all the final state particles are detected and reconstructed into the signal candidate but one or more are misidentified as the wrong type of particle.

\subsection{Fully reconstructed backgrounds} \label{sec:fully-reconstructed-backgrounds}

\subsubsection{\boldmath\BdDpi} \label{sec:BdDpi_background}

There are two possible ways in which the \BdDpi background can contribute to the total background.
In the case where the \D meson decays to a final state of $\Kp\pim\pim$,
if one of the \pim are misidentified as a \Km then this will create a background to \BsDspi for the case when the \Ds meson decays to $\Kp\Km\pim$.

Moreover, considering the same final state of the \D meson ($\Kp\pim\pim$), if the \Kp is misidentified as a \pip
and also one of the \pim are misidentified as a \Km then this will be reconstructed as a \Ds ($\pip\Km\pim$)
which is also one of the desired final states of \BsDspi.

Since both of these background sources contain a \D meson which has been misidentified as a \Ds meson
and the \Bs mass was reconstructed with the invariant mass of the \Ds meson daughters fixed to the \Ds meson mass,
the background will peak underneath the signal.

It is therefore important to understand the shape and event yield for this background to avoid it being absorbed into the signal yield.
A sample of \BdDpi from data is selected using the correct \D meson mass hypothesis.
These events are then reconstructed again as \BsDspi using the incorrect \Ds mass hypothesis,
taking into account the expected shift in momentum distribution caused by the differing particle identification requirements.
The resulting distribution is fitted with a smooth PDF (a sum of Gaussian kernels) using \roofit's \texttt{RooKeysPdf}
and is shown in Figure~\ref{fig:templatesBdDpi}.

\begin{figure}[htb]
 \centering
 \includegraphics[width=.75\textwidth]{\currfiledir/template_Bs2DsPi_Bd2DPi_down_kpipi.pdf}
 %\includegraphics[width=.45\textwidth]{\currfiledir/template_Bs2DsPi_Bd2DPi_up_kpipi.pdf}
 \caption{Mass distribution and the sum of Gaussian kernels for the \BdDpi background from the magnet down sample.}
 \label{fig:templatesBdDpi}
\end{figure}

\subsubsection{\boldmath\BdDspi}

It is possible for the \Bd meson to decay to the same finals state as the signal mode and is kinematically similar.
Therefore, it is not possible to use particle identification requirements to reduce it.

The mass shape for this background is taken from simulated events and fitted with a Double Crystal Ball
(two Crystal Ball functions \cite{CrystalBall} sharing a common mean with tails pointing in opposite directions).
The Crystal Ball function is defined as
\begin{equation}
 f(x;\alpha,n,\bar x,\sigma) = N \cdot \begin{cases} \exp(- \frac{(x - \bar x)^2}{2 \sigma^2}), & \mbox{for }\frac{x - \bar x}{\sigma} > -\alpha \\
                               A \cdot (B - \frac{x - \bar x}{\sigma})^{-n}, & \mbox{for }\frac{x - \bar x}{\sigma} \leqslant -\alpha \end{cases}
\end{equation}
where
\begin{align}
A &= \left(\frac{n}{\left| \alpha \right|}\right)^n \cdot \exp\left(- \frac {\left| \alpha \right|^2}{2}\right), \\
B &= \frac{n}{\left| \alpha \right|}  - \left| \alpha \right|, \\
N &= \frac{1}{\sigma (C + D)}, \\
C &= \frac{n}{\left| \alpha \right|} \cdot \frac{1}{n-1} \cdot \exp\left(- \frac {\left| \alpha \right|^2}{2}\right), \\
D &= \sqrt{\frac{\pi}{2}} \left(1 + \operatorname{erf}\left(\frac{\left| \alpha \right|}{\sqrt 2}\right)\right),
\end{align}
$N$ is a normalization factor, $\sigma$ is the mean of the core Gaussian function and $\alpha$, $n$ and $\bar{x}$ are parameters which are fitted to the data.

\subsubsection{\boldmath\LbLcpi}

The final fully reconstructed background considered is that of \LbLcpi.
This background arises through the misreconstruction of the \Lc as a \Ds largely due to misidentifying the proton as a kaon in the final state
and so shows as a background to the \DsKKpi final state.
The simulated data and the sum of Gaussian kernels fit to it is shown in Figure~\ref{fig:templates-BsDsPi-lambda}.

\begin{figure}[htb]
 \centering
 \includegraphics[width=.75\textwidth]{\currfiledir/template_Bs2DsPi_Lb2LcPi_both.pdf}
 \caption{Mass distribution and the sum of Gaussian kernels for the \LbLcpi background.}
 \label{fig:templates-BsDsPi-lambda}
\end{figure}

\subsection{Partially reconstructed backgrounds}

Partially reconstructed backgrounds are those where a particle is missed and there may have been one or more misidentifications.
They generally peak in lower mass range (except the \Lb) but may extend to the signal region.
These backgrounds can be grouped into three main sub-categories: low-mass \Bs, low-mass \Bd and \Lb decays.

The shapes of all backgrounds in this category are all modelled using simulated data and parameterised with a sum of Gaussian kernels.

\subsubsection{Low-mass \boldmath\Bs}

There are three backgrounds in this group, all due to the decay of a \Bs meson.
Decays of \BsDsrho could show as signal if either the $\rho$ is missed in the reconstruction with a background pion selected in its place or if the $\rho$ decays to $\pip\piz$. If the \piz is missed and the \pip carries most of the momentum then the reconstructed \Bs will peak near the $\Ds\pion$ mass.
In the case where the \Ds meson is produced via a \Dss, the intermediate state of \BsDsstpi could emit a photon which is then missed by the detector.
In the case that both of these occur in the same event, a \BsDsstrho could appear as the signal and so will provide a background.
The templates used in the mass fit are shown in Figure~\ref{fig:templates-BsDsPi1}.

\begin{figure}[htb]
 \centering
 \includegraphics[width=.49\textwidth]{\currfiledir/template_Bs2DsPi_Bs2DsRho_both.pdf}
 \includegraphics[width=.49\textwidth]{\currfiledir/template_Bs2DsPi_Bs2DsstPi_both.pdf}
 \includegraphics[width=.49\textwidth]{\currfiledir/template_Bs2DsPi_Bs2DsstRho_both.pdf}
 \caption[]{Mass distribution and the sum of Gaussian kernels.
   Top left: \BsDsrho. Top right: \BsDsstpi.
   Bottom: \BsDsstrho.}
 \label{fig:templates-BsDsPi1}
\end{figure}

\subsubsection{Low-mass \boldmath\Bd}

There are three backgrounds in this group: \BdDrho, \BdDsstpi and \BdDstpi.
These backgrounds occur for similar reasons to the low-mass \Bs above; a combination of missed particles and misidentified particle types causing a \D or \Dstar to be misreconstructed as a \Ds.
The distributions and the fitted templates are shown in Figure~\ref{fig:templates-BsDsPi2}.

\begin{figure}[htb]
 \centering
 \includegraphics[width=.49\textwidth]{\currfiledir/template_Bs2DsPi_Bd2DRho_both.pdf}
 \includegraphics[width=.49\textwidth]{\currfiledir/template_Bs2DsPi_Bd2DsstPi_both.pdf}
 \includegraphics[width=.49\textwidth]{\currfiledir/template_Bs2DsPi_Bd2DstPi_both.pdf}
 \caption{Mass distribution and the sum of Gaussian kernels.
   Top left: \BdDrho. Top right: \BdDsstpi.
   Bottom: \BdDstpi.}
 \label{fig:templates-BsDsPi2}
\end{figure}

\subsection{Combinatorial background} \label{sec:combinatorial_background}

The final background to consider is of the combinatorial nature.
This arises due to the reconstruction algorithm choosing random background tracks and combining them while reconstructing a \Bs candidate.
Due to the momentum distribution of the background particles, the reconstructed \Bs mass will follow an approximate exponential distribution, peaking at lower masses and reducing towards higher masses.

The templates for the mass fit are extracted from \BsDspi data using the sidebands of the \Bs and \Ds mass distributions.
Events with $m(\Ds)=(1868,1948)\cup(1990,2068) \mevcc$ and $m(\Bs) > \unit{5600}{\mevcc}$ are used.
A fit of an exponential function to this data is then extrapolated back over the whole \Bs mass range.
Each \Ds final state is fitted separately to produce an independent mass template.
The data distributions with their fitted functions are shown in Figure~\ref{fig:combinatorial_slopes} and the parameter values are displayed in Table~\ref{tab:combinatorial_slopes}.

\begin{figure}[htb]
 \centering
 \includegraphics[width=.49\textwidth]{\currfiledir/Comb_Bs2DsPi_KKPi.pdf}
 \includegraphics[width=.49\textwidth]{\currfiledir/Comb_Bs2DsPi_KPiPi.pdf}
 \includegraphics[width=.49\textwidth]{\currfiledir/Comb_Bs2DsPi_PiPiPi.pdf}
  \caption{Combinatorial background slopes evaluated from the \Ds and \Bs sidebands. 
   Top left: \DsKKpi. Top right: \DsKpipi.
   Bottom: \Dspipipi.}
 \label{fig:combinatorial_slopes}
\end{figure}

\begin{table}[htb]
  \centering
  \begin{tabular}{cS}
  \toprule
  \textbf{\Ds mode}  &  \textbf{Slope parameter ($c^2/\mev$)} \\
  \midrule
  \DsKKpi            &  -1.91  $\pm$ 0.10 \\
  \DsKpipi           &  -1.35  $\pm$ 0.14 \\
  \Dspipipi          &  -1.26  $\pm$ 0.11 \\
  \bottomrule
  \end{tabular}
  \caption{The fitted values (in units of $10^{-3}$) of the slope parameter of an exponential function describing the combinatorial background.}
  \label{tab:combinatorial_slopes}
\end{table}

\section{Mass fit} \label{sec:mass-fit}

\subsection{Signal shape}

The signal is described with a double Crystal Ball function in the same way as for the \BsDsK analysis.
The shapes of these functions are fixed using a fit to simulated data which has had the full selection applied to it.
All shape parameters are floated freely in the fit and the results are given in Table~\ref{tab:doubleCB}.
The resultant PDF and the simulated data it was fitted to are shown in Figure~\ref{fig:signal_lineshape}.
In the final mass fit to data, the tail parameters remain fixed but the widths and means are floated.

\begin{table}[htb]
 \centering
 \begin{tabular}{cc}
  \toprule
  \textbf{Parameter}  &  \textbf{Fitted value}  \\
  \midrule
  $\mu_{\rm DOWN}$    &  5366.3  $\pm$ 0.1 \mevcc \\
  $\mu_{\rm UP}$      &  5366.3  $\pm$ 0.1 \mevcc  \\
  $\sigma_1$          &  12.69   $\pm$ 0.09 \mevcc  \\ 
  $\sigma_2$          &  20.5   $\pm$ 0.3 \mevcc  \\
  ${\alpha}_1$        &  2.13   $\pm$ 0.02 \mevcc   \\
  ${\alpha}_2$        &  -2.06   $\pm$ 0.08 \mevcc  \\
  $n_1$               &  1.10   $\pm$ 0.02   \\
  $n_2$               &  5.8     $\pm$ 1.3   \\
  $f$                 &  0.78   $\pm$ 0.01 \\
  \bottomrule
 \end{tabular}
 \caption{Parameters for the sum of the two Crystal Ball functions describing the signal
          shapes of \BsDspi, obtained from simulated data.}
 \label{tab:doubleCB}
\end{table}

\begin{figure}
  \centering
  \includegraphics[width=\textwidth]{\currfiledir/Signal_Bs2DsPi_both.pdf}
  \caption[Signal mass shapes of \BsDspi evaluated on simulated data.]{Signal mass shapes of \BsDspi evaluated on simulated data.
           The solid lines correspond to the fit to the double Crystal Ball function,
           while the dashed lines correspond to the individual Crystal Ball components.
           The bottom plot show the deviation of the data from the fit line based on statistical uncertainty.}
  \label{fig:signal_lineshape}
\end{figure}

\section{Event yields}

The total mass fit to the data includes all the mass templates defined in the previous section.
The various backgrounds are arranged into five groups.

% \begin{table}[htb]
%  \centering
%  \begin{tabular}{cl}
%   \textbf{Group} & \textbf{Channels} \\
%   \midrule
%   1 & \BsDsstpi, \BsDsrho, \BsDsstrho \\ Low-mass \Bs
%   2 & \BdDrho,  \BdDstpi, \BdDspi, \BdDsstpi \\ Low-mass \Bd
%   3 & \BdDpi \\
%   4 & \LbLcpi \\
%   5 & Combinatorial background
%  \end{tabular}
%  \caption{Definition of background groups in the \BsDspi mass fit.}
%  \label{tab:bgGroupsBsDsPi}
% \end{table}

The first group contains the low-mass partially reconstructed \Bs backgrounds.
The backgrounds in this group are combined into a single PDF,
\begin{equation}
\label{eg:group1_bsdspi}
f_{11} \pdf_{\BsDsstpi} + ( 1 - f_{11} )[ f_{12}\pdf_{\BsDsrho} + ( 1 - f_{12}) (\pdf_{\BsDsstrho}) ].
\end{equation}
where $f_{1N}$ are the relative fractions of the background yields.
These relative fractions are freely floated and shared across all magnet polarities and \Ds final states.

The second group contains the low-mass partially reconstructed \Bd backgrounds as well as the fully reconstructed \BdDspi background.
The event yields of \BdDrho and \BdDstpi are fixed to be $\nicefrac{1}{3.5}$ and $\nicefrac{1}{4}$ of the \BdDpi yield respectively based on their relative experimental branching fractions \cite{PDG}.
The \BdDspi and \BdDsstpi yields are both constrained to be equal to $\nicefrac{1}{30}$ of \BsDspi \cite{PDG}.
The fixed yields for these two groups are shown in Table~\ref{tab:constrBsDsPi}.

The third group consists of just the \BdDpi background mode. The shape and yield of this background is fixed on data as described in Section~\ref{sec:BdDpi_background} and is shown in Table~\ref{tab:constrBsDsPi}.

\begin{table}[htb]
  \centering
  \begin{tabular}{lSS}
  \toprule
\textbf{Mode}         &   \textbf{Magnet up}    &  \textbf{Magnet down} \\     
\midrule
\BdDpi                &                         &                       \\
  $\quad$ \DsKKpi     &   260                   &  363                  \\
  $\quad$ \DsKpipi    &   28                    &  38                   \\
  $\quad$ \Dspipipi   &   0                     &  0                    \\
\midrule
\BdDrho               &                         &                       \\
  $\quad$ \DsKKpi     &   74                    &   104                 \\
  $\quad$ \DsKpipi    &   8                     &   11                  \\
  $\quad$ \Dspipipi   &   0                     &   0                   \\
\midrule
\BdDstpi              &                         &                       \\
  $\quad$ \DsKKpi     &   65                    &   91                  \\
  $\quad$ \DsKpipi    &   7                     &   10                  \\
  $\quad$ \Dspipipi   &   0                     &   0                   \\
\midrule
\LbLcpi               &                         &                       \\
  $\quad$ \DsKpipi    &   0                     &   0                   \\
  $\quad$ \Dspipipi   &   0                     &   0                   \\
  \bottomrule
  \end{tabular}
  \caption{Constrained yields in the \BsDspi mass fit.}
  \label{tab:constrBsDsPi}
\end{table}

Then the \LbLcpi misidentified background is considered.
Since the branching fraction for this decay is very large, the yield is not fixed.
Instead it is floated freely in the fit to the \DsKKpi mode and set to zero for the other two since for it be a background in those modes would require a double misidentification.

The final group contains the combinatorial background and is considered separately for each \Ds final state as described in Section~\ref{sec:combinatorial_background}.

The fitted values of the parameters in the fit are shown in Table~\ref{tab:fitmassBsDsPi} and plots of the fit are shown in Figure~\ref{fig:fit-BsDsPi}.

\begin{table}
  \centering
  \begin{tabular}{lS}
  \toprule
\textbf{Parameter}       &  \textbf{Fitted value}    \\
\midrule 
mean                     &  5370.00  \pm  0.19 \mevcc     \\
$\sigma_{1}$             &  14.95    \pm  0.13 \mevcc     \\
$\sigma_{2}$             &  27.20    \pm  0.85 \mevcc     \\
$f_{11}$                 &  0.904   \pm  0.015    \\
$f_{12}$                 &  0.861   \pm  0.055    \\
\midrule
\DsKKpi Magnet up        &                           \\
  $\quad$ $N_{\BsDspi}$  &  9180     \pm  108       \\ 
  $\quad$ $N_{Comb}$     &  1460     \pm  75        \\
  $\quad$ $N_{Group1}$   &  11291    \pm  118       \\
  $\quad$ $N_{\LbLcpi}$  &  408      \pm  65        \\
\midrule
\DsKKpi magnet down      &                           \\
  $\quad$ $N_{\BsDspi}$  &  13007    \pm  129       \\
  $\quad$ $N_{Comb}$     &  1963     \pm  87        \\
  $\quad$ $N_{Group1}$   &  15472    \pm  139       \\
  $\quad$ $N_{\LbLcpi}$  &  669      \pm  79        \\
\midrule
\DsKpipi magnet up       &                           \\
  $\quad$ $N_{\BsDspi}$  &  727      \pm  29        \\
  $\quad$ $N_{Comb}$     &  260      \pm  25        \\
  $\quad$ $N_{Group1}$   &  891      \pm  34        \\
\midrule
\DsKpipi magnet down     &                          \\
  $\quad$ $N_{\BsDspi}$  &  1058     \pm  35        \\
  $\quad$ $N_{Comb}$     &  361      \pm  30        \\
  $\quad$ $N_{Group1}$   &  1268     \pm  41        \\
\midrule
\Dspipipi magnet up      &                           \\
  $\quad$ $N_{\BsDspi}$  &  1679     \pm  43        \\
  $\quad$ $N_{Comb}$     &  520      \pm  35        \\
  $\quad$ $N_{Group1}$   &  1917      \pm  50        \\
\midrule
\Dspipipi magnet down    &                           \\
  $\quad$ $N_{\BsDspi}$  &  2314     \pm  51        \\
  $\quad$ $N_{Comb}$     &  734      \pm  42        \\
  $\quad$ $N_{Group1}$   &  2581      \pm  58       \\
  \bottomrule
  \end{tabular}
  \caption[Fitted values of the parameters for the \BsDspi signal mass fit.]{Fitted values of the parameters for the \BsDspi signal mass fit.
    The $N_{i}$ are the yields of the signal and background contributions. 
    Mean and width are the parameters of the double Crystal Ball used to
    describe the signal. The parameters $f_i$ are fractions between 
    modes in the group 1 backgrounds: \BsDsrho, \BsDsstrho and \BsDsstpi.}
  \label{tab:fitmassBsDsPi}
\end{table}
%logfile: /afs/cern.ch/work/a/adudziak/public/logfiles/log_mass_dspi_220812.txt

\begin{figure}[htb]
 \centering
 \includegraphics[angle=-90,width=.49\textwidth]{\currfiledir/mass_BsDsPi_up_kkpi.pdf}
 \includegraphics[angle=-90,width=.49\textwidth]{\currfiledir/mass_BsDsPi_down_kkpi.pdf}
 \includegraphics[angle=-90,width=.49\textwidth]{\currfiledir/mass_BsDsPi_up_kpipi.pdf}
 \includegraphics[angle=-90,width=.49\textwidth]{\currfiledir/mass_BsDsPi_down_kpipi.pdf}  
 \includegraphics[angle=-90,width=.49\textwidth]{\currfiledir/mass_BsDsPi_up_pipipi.pdf}
 \includegraphics[angle=-90,width=.49\textwidth]{\currfiledir/mass_BsDsPi_down_pipipi.pdf}
 \includegraphics[width=1.0\textwidth]{\currfiledir/mass_BsDsPi_both_all.pdf} 
 \caption[Result of the simultaneous fit to the \BsDspi candidates.]{Result of the simultaneous fit to the \BsDspi candidates,
   magnet up left, magnet down right, for \DsKKpi (top),
   \DsKpipi (middle), and \Dspipipi (bottom).
   The simultaneous fit to the \BsDspi candidates for both polarities and all
   \Ds final states combined is shown at the very bottom.}
 \label{fig:fit-BsDsPi}
\end{figure}

% workspace: /afs/cern.ch/work/a/adudziak/public/workspace/WS_Mass_DsPi_220812.root

% \begin{table}[htb]
%  \centering
%  \begin{tabular}{l|ccc}
%   \textbf{\Ds Mode}  & \textbf{Magnet up}  & \textbf{Magnet down}  & \textbf{Magnet up \& down} \\
%   \midrule
%   \DsKKpi            & 1.350               &  1.489                & -      \\
%   \DsKpipi           & 0.699               &  0.803                & -      \\
%   \Dspipipi          & 0.823               &  0.852                & -      \\
%   all modes          & -                   & -                     & 1.760
%  \end{tabular}
%  \caption{\chisq probabilities for the \BsDspi mass fit.}
%  \label{tab:chi2massBsDsPi}
% \end{table}
