\chapter{Summary} \label{chapter:summary}

The results of the fit to \BsDspi data are
\begin{alignat}{3}
 \avgs   &=  &&0.197 \pm 0.150 \pm 0.025, \\ %0.152
 \avgd   &= -&&0.888 \pm 0.098 \pm 0.541, \\ %0.500
 \deltas &=  &&0.066 \pm 0.083 \pm 0.004, \\ %0.083
 \deltad &= -&&0.062 \pm 0.050 \pm 0.169,    %0.176
\end{alignat}
where the first uncertainty is statistical and the second is systematic.

All the parameters are within $2\sigma$ of zero and \deltas and \deltad are less than $1\sigma$.
At this level of uncertainty, there is no evidence of non-flavour-specific decays of \BsDspi.

Furthermore, \deltad is related to the difference in the effective lifetimes of the $\Dsm\pip$ and $\Dsp\pim$ final states.
If the above result is argued to state that $\left| \deltad \right| < 0.1$ then it is possible to put a constraint on the difference in effective lifetimes between these two final states.
The effective lifetime is given by
\begin{equation}
 \tau_{\mathrm{eff}} = \tau_{\Bs} \left( 1 + \Dpar \times y_s \right),
\end{equation}
where
\begin{equation}
 y_s = \frac{\DGs}{2\Gs} = 0.09.
\end{equation}
Therefore, taking the difference between the effective lifetimes,
\begin{equation}
 \frac{\Delta \tau_{\mathrm{eff}}}{\tau_{\Bs}} = 2 \deltad \times y_s,
\end{equation}
and using the measured value of \deltad we find
\begin{equation}
 \left| \frac{\Delta \tau_{\mathrm{eff}}}{\tau_{\Bs}} \right| \lesssim 0.02.
\end{equation}
This shows that the $\Dsm\pip$ and the $\Dsp\pim$ have the same lifetime to better than $2\%$.
In the case that the \BsDspi decay is assumed to be flavour-specific, this provides a test of $CPT$ invariance which predicts that particles and anti-particles have equal lifetimes.

Comparing these results to those obtained from the \BsDsK analysis, there are a few notable differences in the results, particularly with respect to the uncertainties.
Firstly, the statistical uncertainties are generally higher across all parameters in \BsDsK and this is simply due to the reduced number of events being fitted.
However, they are also much more consistent, with all four statistical uncertainties (and systematic uncertainties) being very similar to each other.

In the \BsDspi results shown above, there is clearly a much larger variation between the uncertainty on each parameter
and this variation is driven largely by the correlations between the underlying \Dpar, \Dbpar, \Spar and \Sbpar.
This is explored in more detail in Appendix~\ref{appendix:error_propagation}.

Using the same method described in the appendix and applying it to the results of the \BsDsK fit (using the correlations given in Table~\ref{tab:sfit_bsdskcorr}),
it is found that the statistical uncertainties of a reparameterised \BsDsK fit would be those given in Table~\ref{tab:dsk_reparam_uncerts}.
Here it is clear that the statistical uncertainties of \avgd and \deltad have been altered considerably, entirely due to the correlation between \Dpar and \Dbpar.

\begin{table}[h]
  \centering
    \begin{tabular}{cS}
      \toprule
      \textbf{Parameters} & \textbf{Statistical uncertainty} \\
      \midrule
      \avgs               &  0.44 \\
      \avgd               &  0.51 \\
      \deltas             &  0.45 \\
      \deltad             &  0.27 \\
      \bottomrule
    \end{tabular}
  \caption{Estimated statistical uncertainties of the reparameterised \BsDsK \CP parameters. \label{tab:dsk_reparam_uncerts}}
\end{table}

It is also of note that in the altered uncertainties given in Table~\ref{tab:dsk_reparam_uncerts}, \avgs and \deltas are still effectively the same as each other,
in contrast to the difference between \avgs and \deltas from \BsDspi shown at the beginning of this section.
This is due to a difference in analysis method leading to a difference in the correlation matrix between \BsDspi and \BsDsK.
In the \BsDspi analysis, \Spar and \Sbpar are highly correlated (see Table~\ref{tab:dspi-corr-stats-reparam}) with each other but in the \BsDsK analysis they are not.
This is caused by the difference in the treatment of \dms.
When fitting \BsDsK, \dms was fixed but when fitting \BsDspi it was floated, allowing it to carry information between \Spar and \Sbpar, increasing the correlation.

It is worth noting that it can be seen that performing the \CP reparameterisation has reduced the overall uncertainty on the result.
Looking at the \BsDsK statistical uncertainties, all four statistical uncertainties are lower than they were compared to the conventional approach while at the same time, removing all correlations between the parameters.
It might be possible that while the statistical uncertainties have shrunk, the systematic uncertainties may have grown.
However, while the latter are large on the \BsDspi results, these are largely due the fact that \dms was not fixed and the parameterisation of the decay-time acceptance rather than the structure of the \CP parameters.

Since the value of \dms was not fixed in the fit to \BsDspi data, it is possible to make a measurement of it.
A value of $\dms = \unit{17.71 \pm 0.06}{\invps} $ was obtained where the uncertainty is statistical.
Comparing this with the currently published world-average of $\unit{17.69 \pm 0.08}{\invps}$ \cite{PDG} shows that they are in agreement.
The latest published results from \lhcb give values of $\unit{17.93 \pm 0.22 \pm 0.15}{\invps}$ \cite{LHCB-PAPER-2013-036} where the first uncertainty is statistical and the second is systematic.
While the measurement from \BsDspi may appear to be very competitive with both of these, no attempt has been made here to quantify the systematic uncertainties on \dms.
