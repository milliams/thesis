#! /usr/bin/python

import sys

def fix_pdf(filename):
	"""
	filename is without .pdf extension
	"""
	from subprocess import call
	command = "pdf2ps {filename}.pdf {filename}_temp.ps && \
	           ps2pdf {filename}_temp.ps {filename}_temp.pdf && \
	           pdfcrop {filename}_temp.pdf {filename}.pdf && \
	           rm --force {filename}_temp.pdf {filename}_temp.ps".format(filename=filename)
	command = "/usr/bin/gs -o {filename}_temp.pdf -dPDFSETTINGS=/prepress -sDEVICE=pdfwrite {filename}.pdf && \
	           mv --force {filename}_temp.pdf {filename}.pdf".format(filename=filename)
	call([command], shell=True)

for pdf in sys.argv[1:]:
	fix_pdf(pdf[:-4])

